package com.apps.msaylica.worktimelog.ui.activities

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.ResultReceiver
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.design.widget.TabLayout
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v4.view.ViewPager
import android.support.v7.widget.SwitchCompat
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast

import com.apps.msaylica.worktimelog.R
import com.apps.msaylica.worktimelog.data.ExportData
import com.apps.msaylica.worktimelog.data.ImportData
import com.apps.msaylica.worktimelog.db.vo.TimeLogRecordVo
import com.apps.msaylica.worktimelog.entity.RecordEntity
import com.apps.msaylica.worktimelog.ui.fragments.AddBreakDialogFragment
import com.apps.msaylica.worktimelog.ui.fragments.CalendarFragment
import com.apps.msaylica.worktimelog.ui.fragments.MonthFragment
import com.apps.msaylica.worktimelog.ui.fragments.TodayFragment
import com.apps.msaylica.worktimelog.ui.fragments.WeekFragment
import com.apps.msaylica.worktimelog.prefs.MyPreferences
import com.apps.msaylica.worktimelog.service.FetchAddressIntentService
import com.apps.msaylica.worktimelog.viewmodel.RecordViewModel
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices

import java.io.File
import java.net.URLConnection
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale


open class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, TodayFragment.OnFragmentInteractionListener, MonthFragment.OnFragmentInteractionListener, WeekFragment.OnFragmentInteractionListener, CalendarFragment.OnFragmentInteractionListener, AddBreakDialogFragment.AddBreakDialogListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private var mFragmentManager: FragmentManager? = null
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var mViewPager: ViewPager? = null
    var isCustomRecord: Boolean = false
    private var todayFragment: TodayFragment? = null
    var isInComplete: Boolean = false
    private var alertDialog: AlertDialog? = null
    private var weekFragment: WeekFragment? = null
    private var monthFragment: MonthFragment? = null
    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null
    var latitude: Double = 0.toDouble()
    var longitude: Double = 0.toDouble()
    private var exportType: Int = 0
    lateinit var determinateProgressBar: ProgressBar
    lateinit var indeterminateProgressBar: ProgressBar
    private var progressLayout: RelativeLayout? = null
    private var progressMsg: TextView? = null
    private var drawer: DrawerLayout? = null
    private var mResultReceiver: AddressResultReceiver? = null
    private var locationItem: MenuItem? = null
    var address: String = ""
    private var calendarFragment: CalendarFragment? = null
    lateinit var mRecordViewModel: RecordViewModel
    private var focusedRecord: RecordEntity? = null
    private val mLayout: View? = null

    val isLocationPermissionGranted: Boolean
        get() = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED

    val timeInMillis: Long
        get() {
            val calendar = Calendar.getInstance()
            return calendar.timeInMillis
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        mRecordViewModel = ViewModelProviders.of(this).get(RecordViewModel::class.java)

        drawer = findViewById(R.id.drawer_layout)
        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener(this)
        locationItem = navigationView.menu.findItem(R.id.nav_location)

        val toggle = object : ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)
                if (isLocationPermissionGranted) {
                    locationItem!!.setIcon(R.drawable.ic_location_on_black_24dp)
                    (navigationView.menu.findItem(R.id.nav_location).actionView as SwitchCompat).isChecked = true
                } else {
                    locationItem!!.setIcon(R.drawable.ic_location_off_black_24dp)
                    (navigationView.menu.findItem(R.id.nav_location).actionView as SwitchCompat).isChecked = false
                }
            }
        }
        drawer!!.addDrawerListener(toggle)
        toggle.syncState()


        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)

        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById<View>(R.id.container) as ViewPager
        mViewPager!!.adapter = mSectionsPagerAdapter
        progressLayout = findViewById(R.id.progressLayout)
        determinateProgressBar = findViewById(R.id.determinateProgress)
        indeterminateProgressBar = findViewById(R.id.indeterminateProgress)
        progressMsg = findViewById(R.id.progressMsg)
        //progressBar.setVisibility(View.INVISIBLE);
        val tabLayout = findViewById<TabLayout>(R.id.tabs)
        tabLayout.setupWithViewPager(mViewPager)

        val fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setImageResource(R.drawable.ic_action_add)
        fab.setOnClickListener { view -> createRecord() }

        MyPreferences(this)

        isInComplete = MyPreferences.getBool(MyPreferences.IS_INCOMPLETE, false)
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        isLocationRequested = MyPreferences.getBool(MyPreferences.LOCATION_ON, true)
        navigationView.menu.findItem(R.id.nav_location).actionView = SwitchCompat(this)

        if (isLocationPermissionGranted) {
            locationItem!!.setIcon(R.drawable.ic_location_on_black_24dp)
            (navigationView.menu.findItem(R.id.nav_location).actionView as SwitchCompat).isChecked = true
        } else {
            locationItem!!.setIcon(R.drawable.ic_location_off_black_24dp)
            (navigationView.menu.findItem(R.id.nav_location).actionView as SwitchCompat).isChecked = false
        }
        val drawerSwitch = navigationView.menu.findItem(R.id.nav_location).actionView as SwitchCompat
        drawerSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                if (!isLocationPermissionGranted) {
                    requestLocationPermission()
                }
            } else {
                locationItem!!.setIcon(R.drawable.ic_location_off_black_24dp)
                MyPreferences.setBool(MyPreferences.LOCATION_ON, false)
            }
        }
    }

    override fun onResume() {
        super.onResume()
    }

    fun createRecord() {
        isCustomRecord = true
        val c = Calendar.getInstance()
        val timeInMillis = c.timeInMillis
        addRecord(timeInMillis)
    }

    /**
     * Inserts record to database
     *
     * @param punchInTime - punch in time in millis
     */
    fun addRecord(punchInTime: Long) {
        val record = RecordEntity()
        record.timeIn = punchInTime
        record.timeOut = punchInTime
        record.timeBreak = 0
        record.latitude = ""
        record.longitude = ""
        record.address = ""
        record.duration = ""
        record.notes = ""
        if (!isCustomRecord) {
            getLocation()
        }

        mRecordViewModel.insertRecord(record)
    }


    private fun getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestLocationPermission()
        } else {
            mFusedLocationClient!!.lastLocation
                    .addOnSuccessListener(this) { location ->
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object
                            recordLocation = location
                            latitude = location.latitude
                            longitude = location.longitude
                            startIntentService(location, "")
                        }
                    }
        }
    }

    open fun requestLocationPermission() {
        // BEGIN_INCLUDE(camera_permission_request)
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)) {
            Snackbar.make(drawer!!, R.string.permission_location_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok) { view ->
                        ActivityCompat.requestPermissions(this@MainActivity,
                                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                                REQUEST_LOCATION)
                    }
                    .show()
        } else {

            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    REQUEST_LOCATION)
        }
    }

    private fun requestWriteToStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            Snackbar.make(mLayout!!, R.string.permission_write_to_storage_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok) { view ->
                        ActivityCompat.requestPermissions(this@MainActivity,
                                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                                REQUEST_WRITE_PERMISSION)
                    }
                    .show()
        } else {

            // Camera permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    REQUEST_WRITE_PERMISSION)
        }
        // END_INCLUDE(camera_permission_request)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {

        if (requestCode == REQUEST_LOCATION) {
            // BEGIN_INCLUDE(permission_result)
            // Received permission result for camera permission.
            Log.i(TAG, "Received response for Location permission request.")

            // Check if the only required permission has been granted
            if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // location has been granted
                Log.i(TAG, "LOCATION permission has now been granted.")
                MyPreferences.setBool(MyPreferences.LOCATION_ON, true)
                locationItem!!.setIcon(R.drawable.ic_location_on_black_24dp)
            } else {
                Log.i(TAG, "LOCATION permission was NOT granted.")
                Snackbar.make(drawer!!, resources.getString(R.string.permissions_not_granted),
                        Snackbar.LENGTH_SHORT).show()
                locationItem!!.setIcon(R.drawable.ic_location_off_black_24dp)
                MyPreferences.setBool(MyPreferences.LOCATION_ON, false)
            }
            // END_INCLUDE(permission_result)
        } else if (requestCode == REQUEST_PERMISSION_READ_EXTERNAL) {

        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
        //        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        //        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
        //            drawerLayout.closeDrawer(GravityCompat.START, true);
        //        }
    }

    fun addBreakTime(timeBreak: Int, record: RecordEntity) {
        focusedRecord = record
        val fm = supportFragmentManager
        val addBreakDialogFragment = AddBreakDialogFragment.newInstance()
        val args = Bundle()
        args.putInt(MainActivity.RECORD_ID, record.recordId)
        args.putInt(MainActivity.TIME_BREAK, timeBreak)
        addBreakDialogFragment.arguments = args
        addBreakDialogFragment.show(fm, "fragment_add_break")
    }


    fun getDay(timeInMillis: Long): String? {
        val calendar = Calendar.getInstance()
        val toDay = SimpleDateFormat("EEEE", Locale.ENGLISH).format(calendar.time)
        calendar.time = Date(timeInMillis)
        var recordDay = SimpleDateFormat("EEEE", Locale.ENGLISH).format(calendar.time)
        if (toDay == recordDay)
            recordDay = "Today"
        return recordDay
    }

    fun getDayShort(timeInMillis: Long): String {
        val calendar = Calendar.getInstance()
        calendar.time = Date(timeInMillis)
        return SimpleDateFormat("d", Locale.ENGLISH).format(calendar.time)
    }

    fun getDayMonth(timeInMillis: Long): String {
        val calendar = Calendar.getInstance()
        calendar.time = Date(timeInMillis)
        return SimpleDateFormat("EEE MMM", Locale.ENGLISH).format(calendar.time)
    }


    fun getDate(durationInMillis: Long): String {

        val calendar = Calendar.getInstance()
        calendar.time = Date(durationInMillis)
        val simpleDateFormatformat = SimpleDateFormat("EEE d MMM yyyy")
        return simpleDateFormatformat.format(calendar.time)
    }

    fun getFormatedTime(durationInMillis: Long): String {

        val calendar = Calendar.getInstance()
        calendar.time = Date(durationInMillis)
        val simpleDateFormatformat = SimpleDateFormat("h:mm a")
        return simpleDateFormatformat.format(calendar.time)
    }

    fun addEditNote(notes: String, record: RecordEntity) {
        val builder = AlertDialog.Builder(this)
        builder.setCancelable(true)
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        //builder.setMessage(notes);
        val inflater = layoutInflater
        val editNoteLayout = inflater.inflate(R.layout.notes_editor_dialog, null)
        builder.setView(editNoteLayout)
        val editText = editNoteLayout.findViewById<View>(R.id.edit_note) as EditText
        var cBtnText: String? = null
        var pBtnText: String? = null
        editText.isCursorVisible = false

        builder.setTitle(getString(R.string.title_add_note))
        cBtnText = getString(R.string.btn_cancel)
        pBtnText = getString(R.string.btn_edit)
        //            editText.requestFocus();
        builder.setPositiveButton(pBtnText, null)
        builder.setNegativeButton(cBtnText) { dialogInterface, i ->
            imm?.hideSoftInputFromWindow(editText.windowToken, 0)
        }

        editText.setText(notes)

        editText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun afterTextChanged(editable: Editable) {
                if (editText.text.toString() != notes) {
                    alertDialog!!.getButton(AlertDialog.BUTTON_POSITIVE).visibility = View.VISIBLE
                    alertDialog!!.getButton(AlertDialog.BUTTON_POSITIVE).text = getString(R.string.btn_save)
                } else {
                    alertDialog!!.getButton(AlertDialog.BUTTON_POSITIVE).text = getString(R.string.btn_edit)
                }
            }
        })
        alertDialog = builder.create()
        //alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setText("");//setVisibility(View.INVISIBLE);
        alertDialog!!.setOnShowListener { dialog ->
            val button = (dialog as AlertDialog).getButton(AlertDialog.BUTTON_POSITIVE)
            button.setOnClickListener { view ->
                if (editText.text.toString() != notes) {
                    imm?.hideSoftInputFromWindow(editText.windowToken, 0)
                    record.notes = editText.text.toString()
                    mRecordViewModel.updateRecord(record)
                    alertDialog!!.dismiss()
                } else {
                    editText.isCursorVisible = true
                    imm?.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
                }
            }
        }

        editText.setOnFocusChangeListener { view, b ->

        }
        alertDialog!!.show()
        editText.clearFocus()

    }

    override fun onBackPressed() {
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    protected fun startIntentService(location: Location?, locationName: String) {
        val handler = Handler()
        mResultReceiver = AddressResultReceiver(handler)
        val intent = Intent(this, FetchAddressIntentService::class.java)
        intent.putExtra(RECEIVER, mResultReceiver)
        intent.putExtra(LOCATION_NAME, locationName)
        intent.putExtra(LOCATION_DATA_EXTRA, location)
        startService(intent)
    }

    override fun onConnected(bundle: Bundle?) {

    }

    override fun onConnectionSuspended(i: Int) {

    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {

    }

    fun showLocation(latitude: String, longitude: String) {
        // Create a Uri from an intent string. Use the result to create an Intent.

        val gmmIntentUri = Uri.parse("geo:$latitude,$longitude")

        // Create an Intent from gmmIntentUri. Set the action to ACTION_VIEW
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        // Make the Intent explicit by setting the Google Maps package
        mapIntent.setPackage("com.google.android.apps.maps")

        // Attempt to start an activity that can handle the Intent
        startActivity(mapIntent)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId
        var msg = ""
        if (id == R.id.nav_export) {
            exportType = LOCAL_EXPORT
            if (ActivityCompat.checkSelfPermission(this@MainActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestWriteToStoragePermission()
            } else {
                msg = getString(R.string.s_exporting)
                val sqlToCsv = ExportData(this@MainActivity)
                sqlToCsv.exportTimeLogs()
            }
        } else if (id == R.id.nav_import) {
            exportType = LOCAL_IMPORT
            val readExternalStoragePermission = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.READ_EXTERNAL_STORAGE)
            if (readExternalStoragePermission != PackageManager.PERMISSION_GRANTED) {
                val requirePermission = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
                ActivityCompat.requestPermissions(this@MainActivity, requirePermission, REQUEST_PERMISSION_READ_EXTERNAL)
            } else {
                browseForFile()
            }
        } else if (id == R.id.nav_share) {
            exportType = EMAIL_EXPORT
            if (ActivityCompat.checkSelfPermission(this@MainActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestWriteToStoragePermission()
            } else {
                val sqlToCsv = ExportData(this@MainActivity)
                sqlToCsv.exportTimeLogs()
            }
        } else if (id == R.id.nav_settings) {
            val settingsIntent = Intent(this, SettingsActivity::class.java)
            startActivity(settingsIntent)
        } else if (id == R.id.nav_location) {
            return false
        }

        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onFinishAddBreakDialog(breakTime: Int, recordId: Int) {
        if (focusedRecord!!.recordId == recordId) {
            focusedRecord!!.timeBreak = breakTime
            mRecordViewModel.updateRecord(focusedRecord!!)
        }
    }

    override fun onFragmentInteraction(uri: Uri) {

    }

    fun exportCompleted(result: File, recordCount: Int) {
        var msg = ""
        when (exportType) {
            MainActivity.LOCAL_EXPORT -> {
                if (recordCount > 0) {
                    msg = getString(R.string.s_exported_total) + " " + recordCount + " " + getString(R.string.s_records)
                }
                showAlertDialog(msg, getString(R.string.title_success))
            }
            MainActivity.LOCAL_IMPORT -> {
                if (recordCount > 0) {
                    msg = getString(R.string.s_imported_total) + " " + recordCount + " " + getString(R.string.s_records)
                }
                showAlertDialog(msg, getString(R.string.title_success))
            }
            MainActivity.EMAIL_EXPORT -> composeEmail("Time Log Export", result)
        }

    }

    fun composeEmail(subject: String, attachment: File) {
        // create new Intent
        val intent = Intent(Intent.ACTION_SEND)
        intent.putExtra(Intent.EXTRA_SUBJECT, subject)
        intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        val uri = FileProvider.getUriForFile(
                this,
                applicationContext
                        .packageName + ".provider", attachment)
        intent.putExtra(Intent.EXTRA_STREAM, uri)
        intent.type = URLConnection.guessContentTypeFromName(uri.toString())
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(Intent.createChooser(intent, "Send/Save Time Log CSV file"))
        }
    }

    fun importCompleted(size: Int) {
        runOnUiThread {
            showHideProgress(HIDE_PROGRESS, null)
            val msg = getString(R.string.s_imported_total) + " " + size + " " + getString(R.string.s_records)
            val title = getString(R.string.t_import_success)
            showAlertDialog(msg, title)
        }
    }

    fun updateProgress(progress: Int) {
        determinateProgressBar.progress = progress
        val text = "$progress%"
        progressMsg!!.text = text
    }

    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    private inner class SectionsPagerAdapter internal constructor(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        init {
            mFragmentManager = fm
        }

        override fun getItem(position: Int): Fragment? {
            when (position) {
                0 -> {
                    todayFragment = TodayFragment()
                    return todayFragment
                }
                1 -> {
                    weekFragment = WeekFragment()
                    return weekFragment
                }
                2 -> {
                    monthFragment = MonthFragment()
                    return monthFragment
                }
                3 -> {
                    calendarFragment = CalendarFragment()
                    return calendarFragment
                }
                else -> return null
            }

        }

        override fun getCount(): Int {
            return 4
        }

        override fun getPageTitle(position: Int): CharSequence? {
            when (position) {
                TimeLogRecordVo.FRAGMENT_TODAY -> return "TODAY"
                TimeLogRecordVo.FRAGMENT_WEEK -> return "WEEK"
                TimeLogRecordVo.FRAGMENT_MONTH -> return "MONTH"
                TimeLogRecordVo.FRAGMENT_CALENDAR -> return "CALENDAR"
            }
            return null
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        var msg = ""
        if (requestCode == BROWSE_CSV_FILE) {
            if (resultCode == Activity.RESULT_OK) {
                var filePath: String? = null
                val _uri = data.data
                Log.d("", "URI = " + _uri!!)
                if (_uri != null) {
                    if (!_uri.path!!.contains(".csv")) {
                        msg = "Please select a file in CSV format."
                        val title = "Error!"
                        showAlertDialog(msg, title)
                    } else {
                        msg = getString(R.string.s_importing)
                        showHideProgress(PROGRESS_INDETERMINATE, msg)
                        filePath = _uri.path!!.replace("/document/raw:", "")
                        val file = File(filePath)
                        val importData = ImportData(this)
                        val recordCount = intArrayOf(0)
                        Thread { importData.importAllRecords(file) }.start()

                        Log.e("Imported: ", recordCount[0].toString() + " records")
                    }
                }
            }
        } else if (requestCode == RECORD_DETAIL) {
            if (resultCode == Activity.RESULT_OK) {
                val handler = Handler()
                handler.postDelayed({
                    Snackbar.make(drawer!!, "RecordEntity Deleted.", Snackbar.LENGTH_LONG)
                            .setAction("OK", null).show()
                }, 400)
            }
        }
    }

    private fun showAlertDialog(msg: String, title: String) {
        val alertDialog = android.support.v7.app.AlertDialog.Builder(this).create()
        alertDialog.setTitle(title)
        alertDialog.setMessage(msg)
        alertDialog.setButton(android.support.v7.app.AlertDialog.BUTTON_NEUTRAL, "OK"
        ) { dialog, which -> dialog.dismiss() }
        alertDialog.show()
    }

    private fun browseForFile() {
        val fileIntent = Intent(Intent.ACTION_GET_CONTENT)
        fileIntent.type = "*/*"
        if (fileIntent.resolveActivity(packageManager) != null) {
            startActivityForResult(fileIntent, BROWSE_CSV_FILE)
        } else {
            Toast.makeText(this, "You do not have an app to perform file browsing.", Toast.LENGTH_LONG).show()
        }
    }

    fun showHideProgress(type: Int, msg: String?) {
        when (type) {
            0 -> {
                indeterminateProgressBar.visibility = View.GONE
                determinateProgressBar.visibility = View.GONE
                progressLayout!!.visibility = View.GONE
            }
            PROGRESS_INDETERMINATE -> {
                indeterminateProgressBar.visibility = View.VISIBLE
                progressMsg!!.text = msg
                progressLayout!!.visibility = View.VISIBLE
                progressMsg!!.visibility = View.VISIBLE
            }
            PROGRESS_DETERMINATE -> {
                progressLayout!!.visibility = View.VISIBLE
                determinateProgressBar.max = 100
                determinateProgressBar.visibility = View.VISIBLE
                progressMsg!!.visibility = View.VISIBLE
                progressMsg!!.text = msg
            }
        }
    }

    fun getTimeDiff(durationInMillis: Long): Double {

        val calendar = Calendar.getInstance()
        calendar.time = Date(durationInMillis)


        val seconds = (durationInMillis / 1000).toInt() % 60
        var minutes = (durationInMillis / (1000 * 60) % 60).toInt()
        val hours = (durationInMillis / (1000 * 60 * 60) % 24).toInt()

        val fSeconds = seconds.toString()

        if (seconds > 30) {
            minutes++
        }

        val realMinute = minutes / 60.0

        // formattedDate have current date/time
        return hours + realMinute
    }

    internal inner class AddressResultReceiver(handler: Handler) : ResultReceiver(handler) {

        override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {

            if (resultData == null) {
                return
            }

            // Display the address string
            // or an error message sent from the intent service.
            var mAddressOutput = resultData.getString(RESULT_DATA_KEY)
            if (mAddressOutput == null) {
                mAddressOutput = ""
            }
            displayAddressOutput(mAddressOutput)

            // Show a toast message if an address was found.
            if (resultCode == SUCCESS_RESULT) {
                showToast(getString(R.string.address_found))
            }

        }


    }

    private fun showToast(string: String) {
        Toast.makeText(this, string, Toast.LENGTH_LONG).show()
    }

    open fun displayAddressOutput(mAddressOutput: String) {
        val fragId = mViewPager!!.currentItem
        if (fragId == TimeLogRecordVo.FRAGMENT_TODAY) {
            address = mAddressOutput
        } else if (fragId == TimeLogRecordVo.FRAGMENT_WEEK) {
        }
        isCustomRecord = false
    }
    fun getTimeInMillis(adjustTimeString: String): Long {
        val sdf = SimpleDateFormat("EEE d MMM yyyy HH:mm")
        var date: Date? = null
        try {
            date = sdf.parse(adjustTimeString)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return date?.time ?: 0
    }

    fun getDateAndTime(durationInMillis: String): String {
        val duration = java.lang.Long.parseLong(durationInMillis)
        val calendar = Calendar.getInstance()
        calendar.time = Date(duration)
        val simpleDateFormatformat = SimpleDateFormat("EEE d MMM yyyy HH:mm a")
        return simpleDateFormatformat.format(calendar.time)
    }

    companion object {
        val RECORD_ID = "record_id"
        val TIME_BREAK = "time_break"
        val NUM_RECORDS_IMPORTED = "records_imported"
        val EXPORT_TYPE = "export_type"
        val LOCAL_EXPORT = 4
        val EMAIL_EXPORT = 5
        val NUM_RECORDS_EXPORTED = "num_records_exported"
        val RECORD_DETAIL = 6
        val LOCATION_NAME = "location_name"
        val NEW_RECORD = -1
        private val TAG = "MainActivity"
        val REQUEST_LOCATION = 1
        private val REQUEST_WRITE_PERMISSION = 2
        private val LOCAL_IMPORT = 3
        private val REQUEST_PERMISSION_READ_EXTERNAL = 4
        private val BROWSE_CSV_FILE = 5
        const val PROGRESS_INDETERMINATE = 1
        const val HIDE_PROGRESS = 0
        const val PROGRESS_DETERMINATE = 2

        const val SUCCESS_RESULT = 0
        const val FAILURE_RESULT = 1
        const val PACKAGE_NAME = "com.apps.msaylica.worktimelog"
        const val RECEIVER = "$PACKAGE_NAME.RECEIVER"
        const val RESULT_DATA_KEY = "$PACKAGE_NAME.RESULT_DATA_KEY"
        const val LOCATION_DATA_EXTRA = "$PACKAGE_NAME.LOCATION_DATA_EXTRA"
        var isDataModified: Boolean = false
        var isLocationRequested: Boolean = false
        private var recordLocation: Location? = null

        fun getTimeInMillis(adjustTimeString: String): Long {
            val sdf = SimpleDateFormat("EEE d MMM yyyy HH:mm")
            var date: Date? = null
            try {
                date = sdf.parse(adjustTimeString)
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            return date?.time ?: 0
        }

        fun round(value: Double, places: Int): Double {
            var value = value
            if (places < 0) throw IllegalArgumentException()

            val factor = Math.pow(10.0, places.toDouble()).toLong()
            value = value * factor
            val tmp = Math.round(value)
            return tmp.toDouble() / factor
        }
    }
}
