package com.apps.msaylica.worktimelog.ui.fragments

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridView
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast

import com.apps.msaylica.worktimelog.R
import com.apps.msaylica.worktimelog.calendar.TimeLogCollection
import com.apps.msaylica.worktimelog.entity.RecordEntity
import com.apps.msaylica.worktimelog.ui.activities.MainActivity
import com.apps.msaylica.worktimelog.ui.activities.RecordDetailActivity
import com.apps.msaylica.worktimelog.ui.adapter.CalendarAdapter

import java.util.ArrayList
import java.util.Calendar

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [CalendarFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [CalendarFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class CalendarFragment : Fragment() {

    private var mParam1: String? = null
    private var mParam2: String? = null

    private var mListener: OnFragmentInteractionListener? = null
    lateinit var cal_month: Calendar
    lateinit var cal_month_copy: Calendar
    private var calendarAdapter: CalendarAdapter? = null
    private var tv_month: TextView? = null
    private var mainActivity: MainActivity? = null
    //  private DataTask dataTask;
    private var endDateInMillis: Long = 0
    private val startDateInMillis: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }

        this.mainActivity = activity as MainActivity?

        cal_month = Calendar.getInstance()
        cal_month.firstDayOfWeek = Calendar.MONDAY
        cal_month_copy = cal_month.clone() as Calendar
        endDateInMillis = cal_month.timeInMillis
        getCalendarEvents(startDateInMillis, endDateInMillis)

    }

    private fun getCalendarEvents(startDateInMillis: Long, endDateInMillis: Long) {
        mainActivity!!.showHideProgress(MainActivity.PROGRESS_INDETERMINATE, getString(R.string.please_wait))
        mainActivity!!.mRecordViewModel.getRecordsByRange(startDateInMillis, endDateInMillis)
                .observe(this, Observer<List<RecordEntity>> {
                    if (it != null) {
                        this.onDataResponse(it)
                    }
                })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val layout = inflater.inflate(R.layout.fragment_calendar, container, false)
        TimeLogCollection.date_collection_arr = ArrayList()
        calendarAdapter = CalendarAdapter(context!!, cal_month, (TimeLogCollection.date_collection_arr as ArrayList<TimeLogCollection>)!!)

        tv_month = layout.findViewById(R.id.tv_month)
        tv_month!!.text = android.text.format.DateFormat.format("MMMM yyyy", cal_month)


        val previous = layout.findViewById<ImageButton>(R.id.ib_prev)
        previous.setOnClickListener { v ->
            if (cal_month.get(Calendar.MONTH) == 4 && cal_month.get(Calendar.YEAR) == 2017) {
                //cal_month.set((cal_month.get(Calendar.YEAR) - 1), cal_month.getActualMaximum(Calendar.MONTH), 1);
                Toast.makeText(context, "Event Detail is available for current session only.", Toast.LENGTH_SHORT).show()
            } else {
                setPreviousMonth()
                refreshCalendar()
            }


        }

        val next = layout.findViewById<ImageButton>(R.id.Ib_next)
        next.setOnClickListener { view ->
            if (cal_month.get(Calendar.MONTH) == 5 && cal_month.get(Calendar.YEAR) == 2018) {
                //cal_month.set((cal_month.get(Calendar.YEAR) + 1), cal_month.getActualMinimum(Calendar.MONTH), 1);
                Toast.makeText(context, "Event Detail is available for current session only.", Toast.LENGTH_SHORT).show()
            } else {
                setNextMonth()
                refreshCalendar()
            }
        }

        val gridview = layout.findViewById<View>(R.id.gv_calendar) as GridView
        gridview.adapter = calendarAdapter
        gridview.setOnItemClickListener { parent, v, position, id ->
            val selectedGridDate = CalendarAdapter.day_string[position]
            (parent.adapter as CalendarAdapter).getPositionList(selectedGridDate, activity!!)
        }
        return layout
    }

    override fun onResume() {
        super.onResume()
    }

    fun onButtonPressed(uri: Uri) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(uri)
        }
    }

    protected fun setNextMonth() {
        if (cal_month.get(Calendar.MONTH) == cal_month.getActualMaximum(Calendar.MONTH)) {
            cal_month.set(cal_month.get(Calendar.YEAR) + 1, cal_month.getActualMinimum(Calendar.MONTH), 1)
        } else {
            cal_month.set(Calendar.MONTH,
                    cal_month.get(Calendar.MONTH) + 1)
        }
    }

    protected fun setPreviousMonth() {
        if (cal_month.get(Calendar.MONTH) == cal_month.getActualMinimum(Calendar.MONTH)) {
            cal_month.set(cal_month.get(Calendar.YEAR) - 1, cal_month.getActualMaximum(Calendar.MONTH), 1)
        } else {
            cal_month.set(Calendar.MONTH, cal_month.get(Calendar.MONTH) - 1)
        }
    }

    fun refreshCalendar() {
        calendarAdapter!!.refreshDays()
        calendarAdapter!!.notifyDataSetChanged()
        tv_month!!.text = android.text.format.DateFormat.format("MMMM yyyy", cal_month)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    fun onDataResponse(list: List<RecordEntity>) {
        TimeLogCollection.date_collection_arr = ArrayList()
        for (record in list) {
            TimeLogCollection.date_collection_arr?.add(TimeLogCollection(record.recordId, record.timeIn, record.timeOut,
                    record.timeBreak, record.longitude!!, record.latitude!!, record.address!!, record.notes!!, record.duration!!))
        }
        refreshCalendar()
        if (mainActivity!!.isCustomRecord) {
            mainActivity!!.isCustomRecord
            val intent = Intent(mainActivity, RecordDetailActivity::class.java)
            intent.putExtra("RecordId", list[0].recordId)
            mainActivity!!.startActivityForResult(intent, MainActivity.RECORD_DETAIL)
            calendarAdapter!!.dismissDialog()
        }
        mainActivity!!.showHideProgress(MainActivity.HIDE_PROGRESS, "")
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment CalendarFragment.
         */
        fun newInstance(param1: String, param2: String): CalendarFragment {
            val fragment = CalendarFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor
