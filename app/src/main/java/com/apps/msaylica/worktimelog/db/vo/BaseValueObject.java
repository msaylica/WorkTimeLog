package com.apps.msaylica.worktimelog.db.vo;

/**
 * Created by msaylica on 2017-06-16.
 */

public class BaseValueObject {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
