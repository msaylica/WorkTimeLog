package com.apps.msaylica.worktimelog.db.vo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by msaylica on 2017-06-16.
 */

public class TimeLogRecordVo extends BaseValueObject implements Parcelable {

    public static final String TABLE_TIME_RECORD = "time_record";
    public static final String RECORD_ID = "record_id";
    public static final String COLUMN_TIME_IN = "time_in";
    public static final String COLUMN_TIME_OUT = "time_out";
    public static final String COLUMN_TIME_BREAK = "time_break";
    public static final String COLUMN_LONGITUDE = "longitude";
    public static final String COLUMN_LATITUDE = "latitude";
    public static final String COLUMN_ADDRESS = "address";
    public static final String COLUMN_RECORD_NOTES = "record_notes";
    public static final String COLUMN_DURATION = "duration";
    public static final String COLUMN_RECORD_WEEK = "record_week";
    public static final String COLUMN_RECORD_YEAR = "record_year";
    public static final String COLUMN_RECORD_EXTRA = "record_extra";
    public static final int FRAGMENT_TODAY = 0;
    public static final int FRAGMENT_WEEK = 1;
    public static final int FRAGMENT_MONTH = 2;
    public static final int FRAGMENT_CALENDAR = 3;

    public int recordId;
    public long timeIn;
    public long timeOut;
    public int timeBreak;
    public String longitude;
    public String latitude;
    public String notes;
    public String duration;
    public String address;
    private String date;

    public TimeLogRecordVo() {

    }

    public String getDuration() {
        return duration;
    }

    public TimeLogRecordVo(int recordId, String date, long timeIn, long timeOut, int timeBreak, String longitude, String latitude
            , String notes, String address) {
        this.date = date;
        this.recordId = recordId;
        this.timeIn = timeIn;
        this.timeOut = timeOut;
        this.timeBreak = timeBreak;
        this.longitude = longitude;
        this.latitude = latitude;
        this.notes = notes;
        this.address = address;

    }

    public final int TIME_IN_UPDATE = 1;
    public final int TIME_OUT_UPDATE = 2;
    public final int NOTE_UPDATE = 3;

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public long getTimeIn() {
        return timeIn;
    }

    public void setTimeIn(long timeIn) {
        this.timeIn = timeIn;
    }

    public long getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(long timeOut) {
        this.timeOut = timeOut;
    }

    public int getTimeBreak() {
        return timeBreak;
    }

    public void setTimeBreak(int timeBreak) {
        this.timeBreak = timeBreak;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

        @Override
        public TimeLogRecordVo createFromParcel(Parcel in) {
            return new TimeLogRecordVo(in);
        }

        @Override
        public TimeLogRecordVo[] newArray(int size) {
            return new TimeLogRecordVo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flag) {
        dest.writeString(this.date);
        dest.writeInt(this.recordId);
        dest.writeLong(this.timeIn);
        dest.writeLong(this.timeOut);
        dest.writeInt(this.timeBreak);
        dest.writeString(this.longitude);
        dest.writeString(this.latitude);
        dest.writeString(this.notes);
        dest.writeString(this.address);
    }

    @Override
    public String toString() {
        return "TimeRecordVo{" +
                "recordDate'" + date + '\''+
                "recordId='" + recordId + '\'' +
                "timeIn='" + timeIn + '\'' +
                "timeOut='" + timeOut + '\'' +
                "timeBreak='" + timeBreak + '\'' +
                "longitude='" + longitude + '\'' +
                "latitude='" + latitude + '\'' +
                "notes='" + notes + '\'' +
                "address" + address + '\'' +
                '}';
    }

    public TimeLogRecordVo(Parcel in) {
        this.date = in.readString();
        this.recordId = in.readInt();
        this.timeIn = in.readLong();
        this.timeOut = in.readLong();
        this.timeBreak = in.readInt();
        this.longitude = in.readString();
        this.latitude = in.readString();
        this.notes = in.readString();
        this.address = in.readString();
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
