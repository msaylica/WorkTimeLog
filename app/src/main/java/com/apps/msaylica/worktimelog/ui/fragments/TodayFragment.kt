package com.apps.msaylica.worktimelog.ui.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextClock
import android.widget.TextView
import android.arch.lifecycle.Observer


import com.apps.msaylica.worktimelog.R
import com.apps.msaylica.worktimelog.db.vo.TimeLogRecordVo
import com.apps.msaylica.worktimelog.entity.RecordEntity
import com.apps.msaylica.worktimelog.ui.activities.MainActivity
import com.apps.msaylica.worktimelog.ui.adapter.RecordAdapter
import com.apps.msaylica.worktimelog.prefs.MyPreferences

import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date


/**
 * A fragment with a Google +1 button.
 * Activities that contain this fragment must implement the
 * [TodayFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [TodayFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TodayFragment : Fragment() {
    private var mParam1: String? = null
    private var mParam2: String? = null

    private var mListener: OnFragmentInteractionListener? = null
    private var punchIn: TextView? = null
    private var txtPunchIn: TextView? = null
    private var punchInTime: String? = null
    private val punchInDate: Date? = null
    private var txtDuration: TextView? = null
    private var isPunchedIn: Boolean = false
    private var pTime: Date? = null
    private var mRecyclerView: RecyclerView? = null
    private var mAdapter: RecordAdapter? = null
    private var mLayoutManager: RecyclerView.LayoutManager? = null
    private var mainActivity: MainActivity? = null
    private val myDataset: List<TimeLogRecordVo>? = null
    private var progressBar: ProgressBar? = null
    private var progressBarStill: ProgressBar? = null
    private var punchOut: TextView? = null
    private var isInComplete: Boolean = false
    private val record: RecordEntity? = null
    private val recordList: List<RecordEntity>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.mainActivity = activity as MainActivity?
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }

        isInComplete = MyPreferences.getBool(MyPreferences.IS_INCOMPLETE, false)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_today, container, false)
        progressBar = view.findViewById(R.id.timeProgress)
        progressBarStill = view.findViewById(R.id.timeProgress_still)
        punchIn = view.findViewById(R.id.btn_punch_in)
        punchOut = view.findViewById(R.id.btn_punch_out)
        punchOut!!.visibility = View.GONE
        txtPunchIn = view.findViewById(R.id.txt_punch_in_time)
        txtDuration = view.findViewById(R.id.txt_duration_time)
        mRecyclerView = view.findViewById(R.id.recyclerview_today)
        mLayoutManager = LinearLayoutManager(context)
        mRecyclerView!!.layoutManager = mLayoutManager
        getTodaysRecord()

        //        List<TimeLogRecordVo> reverseData = myDataset.subList(0, myDataset.size());
        //        Collections.reverse(reverseData);
        mAdapter = RecordAdapter(activity!!, this)
        mRecyclerView!!.adapter = mAdapter
        punchIn!!.setOnClickListener { view1 -> punchInOut() }
        punchOut!!.setOnClickListener { view1 -> punchInOut() }
        if (isInComplete) {

        } else {
            updateDurationTime(0)
        }
        return view
    }

    private fun getTodaysRecord() {
        val cal = Calendar.getInstance()
        cal.set(Calendar.HOUR_OF_DAY, 0) // ! clear would not reset the hour of day !
        cal.clear(Calendar.MINUTE)
        cal.clear(Calendar.SECOND)
        cal.clear(Calendar.MILLISECOND)
        var year = cal.get(Calendar.YEAR)
        var month = cal.get(Calendar.MONTH)
        var day = cal.get(Calendar.DAY_OF_MONTH)
        cal.set(year, month, day)
        val todayInMillis = cal.timeInMillis
        cal.add(Calendar.DAY_OF_MONTH, 1)
        cal.set(Calendar.HOUR_OF_DAY, 0) // ! clear would not reset the hour of day !
        cal.clear(Calendar.MINUTE)
        cal.clear(Calendar.SECOND)
        cal.clear(Calendar.MILLISECOND)
        year = cal.get(Calendar.YEAR)
        month = cal.get(Calendar.MONTH)
        day = cal.get(Calendar.DAY_OF_MONTH)
        cal.set(year, month, day)
        val nextDayInMillis = cal.timeInMillis
        mAdapter = RecordAdapter(activity!!, this)
        mainActivity!!.mRecordViewModel.getTodaysRecords(todayInMillis, nextDayInMillis)
                .observe(activity!!, Observer<MutableList<RecordEntity>> { recordEntities ->
                    if (recordEntities != null && !recordEntities!!.isEmpty()) {
                        latestRecord = recordEntities!!.get(0)
                    }
                    if (MyPreferences.getBool(MyPreferences.IS_INCOMPLETE, false) && recordEntities!!.isNotEmpty()) {
                        recordEntities.remove(recordEntities.get(0))
                    }
                    mAdapter!!.updateData(recordEntities!!)

                })
    }

    /***
     * Punch in if not punched in yet
     * Punch in if punched in
     */
    private fun punchInOut() {
        val c = Calendar.getInstance()
        val recordWeek = c.get(Calendar.WEEK_OF_YEAR)
        val timeInMillis = c.timeInMillis

        val df = SimpleDateFormat("h:mm:ss a ")
        pTime = c.time
        this.punchInTime = df.format(c.time)
        // formattedDate have current date/time
        txtPunchIn!!.text = mainActivity!!.getFormatedTime(timeInMillis)

        if (isPunchedIn) {//punch out
            progressBar!!.visibility = View.GONE
            progressBarStill!!.visibility = View.VISIBLE
            punchOut!!.visibility = View.GONE
            punchIn!!.visibility = View.VISIBLE
            isPunchedIn = false
            mainActivity!!.isInComplete = false
            txtPunchIn!!.text = "-:--"
            txtDuration!!.text = "00:00:00"
            MyPreferences.setBool(MyPreferences.IS_INCOMPLETE, false)
            latestRecord!!.timeOut = timeInMillis
            latestRecord!!.latitude = mainActivity!!.latitude.toString()
            latestRecord!!.longitude = mainActivity!!.longitude.toString()
            latestRecord!!.address = mainActivity!!.address

            mainActivity!!.mRecordViewModel.updateRecord(latestRecord!!)
        } else {
            progressBarStill!!.visibility = View.GONE
            progressBar!!.visibility = View.VISIBLE
            punchOut!!.visibility = View.VISIBLE
            punchIn!!.visibility = View.GONE
            isPunchedIn = true
            mainActivity!!.isInComplete = true
            mainActivity!!.addRecord(timeInMillis)
            MyPreferences.setBool(MyPreferences.IS_INCOMPLETE, true)
        }
        updateDurationTime(0)
    }


    override fun onResume() {
        super.onResume()
        //updateTime();
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    fun createdNewCustomRecord(mainActivity: MainActivity, record: RecordEntity) {
        val adapter = mAdapter
        //ToDo   TimeLogRecordVo newRecord = mainActivity.getLatestRecord();
        //        myDataset.add(0, newRecord);
        ////      adapter.notifyItemChanged(0);
        //        adapter.focusOnNewRecord(mRecyclerView);
        //        adapter.refreshData(mainActivity);
        //        adapter.notifyDataSetChanged();
        //        adapter.changeNewRecordTimeIn();
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    private fun getTimeDiff(timeIn: Long) {
        activity!!.runOnUiThread(object : Runnable {
            internal var diff: Long = 0
            internal var c = Calendar.getInstance()

            override fun run() {


                val currentDate = c.time
                if (timeIn != 0L) {
                    pTime!!.time = timeIn
                    diff = currentDate.time - pTime!!.time
                } else
                    diff = currentDate.time - pTime!!.time

                //long days = hours / 24;

                val seconds = (diff / 1000).toInt() % 60
                val minutes = (diff / (1000 * 60) % 60).toInt()
                val hours = (diff / (1000 * 60 * 60) % 24).toInt()

                var fSeconds = seconds.toString()
                var fMinutes = minutes.toString()
                var fHours = hours.toString()

                if (seconds < 10) {
                    fSeconds = "0$seconds"
                }
                if (minutes < 10) {
                    fMinutes = "0$minutes"
                }
                if (hours < 10) {
                    fHours = "0$hours"
                }


                // formattedDate have current date/time
                val tDiff = "$fHours:$fMinutes:$fSeconds"
                txtDuration!!.text = tDiff
            }
        })
    }

    @Synchronized
    private fun updateDurationTime(timeIn: Long) {
        Thread {
            try {
                while (isPunchedIn) {
                    if (timeIn != 0L) {
                        getTimeDiff(timeIn)
                    } else {
                        getTimeDiff(0)
                    }
                    Thread.sleep(1000)
                }
            } catch (e: InterruptedException) {
                println(e.message)
            }
        }.start()
    }

    @Synchronized
    private fun updateTime() {

        Thread {
            try {
                while (!isPunchedIn && null != txtPunchIn) {
                    val c = Calendar.getInstance()
                    // txtPunchIn.setText(mainActivity.getFormatedTime(c.getTimeInMillis()));
                    Thread.sleep(1000)
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        val textClock = TextClock(context)
                        txtPunchIn!!.text = textClock.format12Hour
                    }
                }
            } catch (e: InterruptedException) {
                println(e.message)
            }
        }.start()
    }

    companion object {
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"
        // The request code must be 0 or greater.
        private val PLUS_ONE_REQUEST_CODE = 0
        private var latestRecord: RecordEntity? = null

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment TodayFragment.
         */
        fun newInstance(param1: String, param2: String): TodayFragment {
            val fragment = TodayFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor
