package com.apps.msaylica.worktimelog.ui.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.apps.msaylica.worktimelog.R
import com.apps.msaylica.worktimelog.custom.AutoResizeTextView
import com.apps.msaylica.worktimelog.entity.RecordEntity
import com.apps.msaylica.worktimelog.helper.TimeHelper
import com.apps.msaylica.worktimelog.ui.activities.MainActivity
import com.apps.msaylica.worktimelog.ui.activities.RecordDetailActivity
import kotlinx.android.synthetic.main.item_detail_dialog.view.*
import java.text.DecimalFormat
import java.util.ArrayList
import java.util.concurrent.TimeUnit

class DialogAdaptorRecord(private val adapter:CalendarAdapter, val context: Context,
                          private val recordList: ArrayList<RecordEntity>) : RecyclerView.Adapter<DialogAdaptorRecord.ViewHolder>() {
    private val mainActivity: MainActivity = context as MainActivity

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_detail_dialog, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val record = recordList[position]
        holder.txtStartTime.text = TimeHelper.getFormatedTime(record.timeIn)
        holder.txtEndTime.text = TimeHelper.getFormatedTime(record.timeOut)
        val breakString = record.timeBreak.toString() + " min"
        holder.txtBreak.text = breakString
        val breakInMillis = TimeUnit.MINUTES.toMillis(record.timeBreak.toLong())
        val durationInMillis = record.timeOut - record.timeIn
        TimeHelper.getTimeDiff(durationInMillis)
        val duration = TimeHelper.round(TimeHelper.getTimeDiff(durationInMillis - breakInMillis), 2).toString() + " hours"
        val df = DecimalFormat("#.##")
        // String wage = "$" + df.format(getWages(durationInMillis - breakInMillis)) + " in wages";
        val dateInMillis = record.timeIn
        var day: String? = ""
        day = TimeHelper.getDay(dateInMillis)
        holder.txtLocation.text = record.address
        holder.txtNote.text = record.notes
        val heading = "$duration hours "
        holder.txtHeading.text = "" + heading
        holder.btnEdit.setOnClickListener { view ->
            adapter.dismissDialog()
            showDetails(record.recordId)
        }
    }

    override fun getItemCount(): Int {
        return recordList.size
    }

    class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        val txtHeading: AutoResizeTextView = view.txt_heading
        val txtStartTime: AutoResizeTextView = view.txt_start_time
        val txtEndTime: AutoResizeTextView = view.txt_end_time
        val txtBreak: AutoResizeTextView = view.txt_break
        val txtNote: TextView = view.txt_note
        val txtLocation: TextView = view.txt_location
        val btnEdit: ImageButton = view.ic_edit
    }

    private fun showDetails(recordId: Int) {
        val intent = Intent(mainActivity, RecordDetailActivity::class.java)
        intent.putExtra("RecordId", recordId)
        mainActivity.startActivityForResult(intent, MainActivity.RECORD_DETAIL)
    }
}
