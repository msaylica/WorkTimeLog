package com.apps.msaylica.worktimelog.ui.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView

import com.apps.msaylica.worktimelog.R
import com.apps.msaylica.worktimelog.ui.activities.MainActivity

/**
 * Created by msaylica on 2017-07-16.
 */

class AddBreakDialogFragment : DialogFragment() {


    private var btnAdd5Min: TextView? = null
    private var btnAdd15Min: TextView? = null
    private var btnAdd30Min: TextView? = null
    private var btnAdd45Min: TextView? = null
    private var btnAdd60Min: TextView? = null
    private var txtBreak: TextView? = null
    private var totalBreak: Int = 0
    private var btnReset: TextView? = null
    private var btnSet: Button? = null
    private var btnCancel: Button? = null
    private var listener: AddBreakDialogFragment.AddBreakDialogListener? = null
    private var recordId: Int = 0
    private var timeBreak: Int = 0

    private val onSetClick = View.OnClickListener {
        listener!!.onFinishAddBreakDialog(totalBreak, recordId)
        this@AddBreakDialogFragment.dismiss()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val args = arguments
        recordId = args!!.getInt(MainActivity.RECORD_ID)
        timeBreak = args.getInt(MainActivity.TIME_BREAK)
        return inflater.inflate(R.layout.fragment_add_break, container)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the EditNameDialogListener so we can send events to the host
            listener = context as AddBreakDialogListener?
        } catch (e: ClassCastException) {
            // The activity doesn't implement the interface, throw exception
            throw ClassCastException(context!!.toString() + " must implement EditNameDialogListener")
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnAdd5Min = view.findViewById<View>(R.id.btn_5_min) as TextView
        btnAdd15Min = view.findViewById<View>(R.id.btn_15_min) as TextView
        btnAdd30Min = view.findViewById<View>(R.id.btn_30_min) as TextView
        btnAdd45Min = view.findViewById<View>(R.id.btn_45_min) as TextView
        btnAdd60Min = view.findViewById<View>(R.id.btn_60_min) as TextView
        btnReset = view.findViewById<View>(R.id.btn_reset) as TextView
        txtBreak = view.findViewById<View>(R.id.txt_break) as TextView
        val timeBreakString = "$timeBreak min"
        txtBreak!!.text = timeBreakString
        btnSet = view.findViewById<View>(R.id.btn_set) as Button
        btnSet!!.setOnClickListener(onSetClick)
        btnCancel = view.findViewById<View>(R.id.btn_cancel) as Button
        btnCancel!!.setOnClickListener { this@AddBreakDialogFragment.dismiss() }
        totalBreak = timeBreak

        btnAdd5Min!!.setOnClickListener { view1 ->
            //add 5 min
            totalBreak += 5
            updateTotal(totalBreak)
        }

        btnAdd15Min!!.setOnClickListener { view12 ->
            //add 15 min
            totalBreak += 15
            updateTotal(totalBreak)
        }

        btnAdd30Min!!.setOnClickListener { view13 ->
            //add 30 min
            totalBreak += 30
            updateTotal(totalBreak)

        }

        btnAdd45Min!!.setOnClickListener { view14 ->
            //add 45 min
            totalBreak += 45
            updateTotal(totalBreak)
        }

        btnAdd60Min!!.setOnClickListener { view15 ->
            //add 60 min
            totalBreak += 60
            updateTotal(totalBreak)
        }

        btnReset!!.setOnClickListener { view16 ->
            //add 60 min
            totalBreak = 0
            updateTotal(totalBreak)
        }

    }

    private fun updateTotal(totalBreak: Int) {
        val totalBreakString = "$totalBreak min"
        txtBreak!!.text = totalBreakString
    }

    interface AddBreakDialogListener {
        fun onFinishAddBreakDialog(inputText: Int, recordId: Int)
    }

    companion object {

        fun newInstance(): AddBreakDialogFragment {
            return AddBreakDialogFragment()
        }
    }


}

private fun View.setOnClickListener(onCancelClick: (View) -> Unit) {

}
