package com.apps.msaylica.worktimelog.ui.adapter

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.apps.msaylica.worktimelog.R
import com.apps.msaylica.worktimelog.entity.RecordEntity
import com.apps.msaylica.worktimelog.ui.activities.MainActivity
import com.apps.msaylica.worktimelog.ui.activities.RecordDetailActivity
import com.apps.msaylica.worktimelog.ui.fragments.MonthFragment

import java.text.DecimalFormat
import java.util.ArrayList
import java.util.Calendar
import java.util.Date
import java.util.concurrent.TimeUnit

/**
 * Created by msaylica on 2017-06-25.
 */

class RecordAdapter// Provide a suitable constructor (depends on the kind of dataset)
(activity: FragmentActivity, private val fragmentObject: Any) : RecyclerView.Adapter<RecordAdapter.ViewHolder>() {
    private val activity: MainActivity
    private var myDataset: List<RecordEntity>? = null
    private var timePickerTitle: String? = null

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal val txtBreak: TextView
        internal val txtNote: TextView
        val txtStartTime: TextView
        internal val txtEndTime: TextView
        val txtHeading: TextView
        internal val txtLocation: TextView
        //  private final View btnDelete;
        val mToolbar: Toolbar


        init {
            mToolbar = itemView.findViewById(R.id.record_toolbar)
            txtHeading = itemView.findViewById(R.id.txt_heading)
            //txtDate = (TextView) itemView.findViewById(R.id.txt_date);
            txtStartTime = itemView.findViewById(R.id.txt_start_time)
            txtEndTime = itemView.findViewById(R.id.txt_end_time)
            txtBreak = itemView.findViewById(R.id.txt_break)
            txtNote = itemView.findViewById(R.id.txt_note)
            txtLocation = itemView.findViewById(R.id.txt_location)
        }
    }

    init {
        this.myDataset = ArrayList()
        this.activity = activity as MainActivity
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): RecordAdapter.ViewHolder {
        // create a new view
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_record, parent, false) as CardView
        // set the view's size, margins, paddings and layout parameters
        val vh = ViewHolder(v)
        vh.mToolbar.inflateMenu(R.menu.menu_record_item)
        return vh
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        val record = myDataset!![position]
        holder.mToolbar.setOnMenuItemClickListener { item ->

            when (item.itemId) {
                R.id.action_edit -> showDetails(myDataset!![position])
                R.id.action_duplicate -> {
                }
                R.id.action_delete -> activity.mRecordViewModel.deleteRecord(record)
            }
            false
        }
        if (record.timeOut != 0L) {
            holder.txtStartTime.text = activity.getFormatedTime(record.timeIn)
            holder.txtEndTime.text = activity.getFormatedTime(record.timeOut)
            val breakString = record.timeBreak.toString() + " min"
            holder.txtBreak.text = breakString
            val breakInMillis = TimeUnit.MINUTES.toMillis(record.timeBreak.toLong())
            val durationInMillis = record.timeOut - record.timeIn
            getTimeDiff(durationInMillis)
            val duration = round(getTimeDiff(durationInMillis - breakInMillis), 2).toString() + " hours"
            val df = DecimalFormat("#.##")
            val wage = "$" + df.format(getWages(durationInMillis - breakInMillis)) + " in wages"
            val dateInMillis = record.timeIn
            var day: String? = ""
            if (fragmentObject is MonthFragment) {
                day = activity.getDate(dateInMillis)
            } else {
                day = activity.getDay(dateInMillis)
            }
            val heading = "$duration on $day"
            holder.txtHeading.text = heading
        }

        holder.txtHeading.setOnClickListener { view -> adjustDateAndTime(record, holder, holder.txtStartTime) }
        holder.txtStartTime.setOnClickListener { view -> adjustTime(record, holder, holder.txtStartTime) }
        holder.txtEndTime.setOnClickListener { view -> adjustTime(record, holder, holder.txtEndTime) }
        holder.txtBreak.setOnClickListener { view -> activity.addBreakTime(record.timeBreak, record) }
        holder.txtNote.text = record.notes
        holder.txtNote.setOnClickListener { view -> activity.addEditNote(record.notes!!, record) }

        if (record.address == null || record.address!!.isEmpty()) {
            holder.txtLocation.text = activity.getString(R.string.str_unknown)
        } else {
            holder.txtLocation.text = record.address
        }

        holder.txtLocation.setOnClickListener { view ->
            if (record.latitude != null && !record.latitude!!.isEmpty()) {
                activity.showLocation(record.latitude!!, record.longitude!!)
            } else {
                //tell user to turn on location
                activity.requestLocationPermission()
            }
        }
    }

    private fun showDetails(dataset: RecordEntity) {
        val intent = Intent(activity, RecordDetailActivity::class.java)
        intent.putExtra("RecordId", dataset.recordId)
        activity.startActivityForResult(intent, MainActivity.RECORD_DETAIL)
    }

    private fun adjustDateAndTime(record: RecordEntity, holder: ViewHolder, txtStartTime: TextView) {
        val mcurrentTime = Calendar.getInstance()
        val timeIn = record.timeIn
        mcurrentTime.timeInMillis = timeIn
        val year = mcurrentTime.get(Calendar.YEAR)
        val month = mcurrentTime.get(Calendar.MONTH)
        val day = mcurrentTime.get(Calendar.DAY_OF_MONTH)
        val hours = mcurrentTime.get(Calendar.HOUR_OF_DAY)
        val minutes = mcurrentTime.get(Calendar.MINUTE)
        val dlg = DatePickerDialog(txtStartTime.context, { datePicker, i, i1, i2 ->
            Log.e("Day: ", "" + i + i2)
            mcurrentTime.set(i, i1, i2, hours, minutes)
            val timeInMillis = mcurrentTime.timeInMillis
            record.timeIn = timeInMillis
            if (activity.isCustomRecord) {
                adjustTime(record, holder, txtStartTime)
            } else {
                activity.mRecordViewModel.updateRecord(record)
            }
        }, year, month, day)
        dlg.setTitle("Pick Day")
        dlg.show()

        dlg.setOnCancelListener { dialogInterface ->
            if (activity.isCustomRecord) {
                activity.isCustomRecord = false
                activity.mRecordViewModel.deleteRecord(record)
            }
        }
    }

    private fun adjustTime(record: RecordEntity, holder: ViewHolder, e: TextView) {
        val mcurrentTime = Calendar.getInstance()
        val hour = mcurrentTime.get(Calendar.HOUR_OF_DAY)
        val minute = mcurrentTime.get(Calendar.MINUTE)
        val mTimePicker: TimePickerDialog
        if (e === holder.txtStartTime) {
            timePickerTitle = activity.getString(R.string.str_select_start_time)
        } else {
            timePickerTitle = activity.getString(R.string.str_select_end_time)
        }
        //
        mTimePicker = TimePickerDialog(e.context, { timePicker, selectedHour, selectedMinute ->
            var selectedMinuteString = selectedMinute.toString()
            if (selectedMinute < 10) {
                selectedMinuteString = "0$selectedMinute"
            }
            var adjustTimeString = "$selectedHour:$selectedMinuteString "
            val adjustTimeMillis: Long
            if (e === holder.txtStartTime) {
                adjustTimeString = activity.getDate(record.timeIn) + " " + adjustTimeString
                adjustTimeMillis = MainActivity.getTimeInMillis(adjustTimeString)
                record.timeIn = adjustTimeMillis
                activity.mRecordViewModel.updateRecord(record)
            } else {
                adjustTimeString = activity.getDate(record.timeOut) + " " + adjustTimeString
                adjustTimeMillis = MainActivity.getTimeInMillis(adjustTimeString)
                record.timeOut = adjustTimeMillis
                activity.mRecordViewModel.updateRecord(record)
            }
            e.text = activity.getFormatedTime(adjustTimeMillis)
        }, hour, minute, false)//Yes 24 hour time
        mTimePicker.setTitle("" + timePickerTitle!!)
        mTimePicker.show()

        mTimePicker.setOnCancelListener { dialogInterface ->
            if (activity.isCustomRecord) {
                activity.isCustomRecord=false
                activity.mRecordViewModel.deleteRecord(record)
            }
        }
    }

    private fun getWages(durationInMillis: Long): Double {
        val hoursWorked = getTimeDiff(durationInMillis)
        val hourlyPay = 20f
        return hoursWorked * hourlyPay
    }

    private fun getTimeDiff(durationInMillis: Long): Double {

        val calendar = Calendar.getInstance()
        calendar.time = Date(durationInMillis)


        val seconds = (durationInMillis / 1000).toInt() % 60
        var minutes = (durationInMillis / (1000 * 60) % 60).toInt()
        val hours = (durationInMillis / (1000 * 60 * 60) % 24).toInt()

        val fSeconds = seconds.toString()

        if (seconds > 30) {
            minutes++
        }

        val realMinute = minutes / 60.0

        // formattedDate have current date/time
        return hours + realMinute
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount(): Int {
        return if (myDataset != null) {
            myDataset!!.size
        } else {
            0
        }
    }

    fun updateData(recordList: MutableList<RecordEntity>?) {
        myDataset = recordList
        notifyDataSetChanged()
    }

    companion object {

        fun round(value: Double, places: Int): Double {
            var value = value
            if (places < 0) throw IllegalArgumentException()

            val factor = Math.pow(10.0, places.toDouble()).toLong()
            value = value * factor
            val tmp = Math.round(value)
            return tmp.toDouble() / factor
        }
    }
}