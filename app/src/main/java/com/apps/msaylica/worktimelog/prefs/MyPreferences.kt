package com.apps.msaylica.worktimelog.prefs

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences

/**
 * Created by msaylica on 2017-10-03.
 */

class MyPreferences(private val activity: Activity) {

    init {
        initialize()
    }

    private fun initialize() {
        sharedPref = activity.getPreferences(Context.MODE_PRIVATE)
    }

    companion object {


        val IS_INCOMPLETE = "is_incomplete"
        val INCOMPLETE_LOG_KEY = "incomplete_log_key"
        val CUSTOM_START_DATE = "custom_start_date"
        val CUSTOM_END_DATE = "custom_end_date"
        val LOCATION_ON = "location_on"
        private var sharedPref: SharedPreferences? = null
        private var editor: SharedPreferences.Editor? = null

        fun setString(key: String, s: String) {
            editor = sharedPref!!.edit()
            editor!!.putString(key, s)
            editor!!.apply()
        }

        fun setInt(key: String, i: Int) {
            editor = sharedPref!!.edit()
            editor!!.putInt(key, i)
            editor!!.apply()
        }

        fun setLong(key: String, l: Long) {
            editor = sharedPref!!.edit()
            editor!!.putLong(key, l)
            editor!!.apply()
        }

        fun setBool(key: String, b: Boolean) {
            editor = sharedPref!!.edit()
            editor!!.putBoolean(key, b)
            editor!!.apply()
        }

        fun getString(key: String, defaultValue: String): String? {
            return sharedPref!!.getString(key, defaultValue)
        }

        fun getInt(key: String, defaultValue: Int): Int {
            return sharedPref!!.getInt(key, defaultValue)
        }

        fun getLong(key: String, defaultValue: Long): Long {
            return sharedPref!!.getLong(key, defaultValue)
        }

        fun getBool(key: String, defaultValue: Boolean): Boolean {
            return if (sharedPref != null)
                sharedPref!!.getBoolean(key, defaultValue)
            else
                false
        }
    }
}
