package com.apps.msaylica.worktimelog.ui.fragments

import android.arch.lifecycle.Observer
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar

import com.apps.msaylica.worktimelog.R
import com.apps.msaylica.worktimelog.entity.RecordEntity
import com.apps.msaylica.worktimelog.ui.activities.MainActivity
import com.apps.msaylica.worktimelog.ui.adapter.RecordAdapter
import com.apps.msaylica.worktimelog.prefs.MyPreferences

import java.util.Calendar

/**
 * A fragment with a Google +1 button.
 * Activities that contain this fragment must implement the
 * [WeekFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [WeekFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class WeekFragment : Fragment() {
    // The URL to +1.  Must be a valid URL.
    private val PLUS_ONE_URL = "http://developer.android.com"
    private var mParam1: String? = null
    private var mParam2: String? = null

    private var mListener: OnFragmentInteractionListener? = null
    private var mRecyclerView: RecyclerView? = null
    private var mAdapter: RecordAdapter? = null
    private var mLayoutManager: RecyclerView.LayoutManager? = null
    private var mainActivity: MainActivity? = null
    private val myDataset: List<RecordEntity>? = null
    private val progressBar: ProgressBar? = null
    private val progressBarStill: ProgressBar? = null
    private var isInComplete: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.mainActivity = activity as MainActivity?

        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }

        isInComplete = MyPreferences.getBool(MyPreferences.IS_INCOMPLETE, false)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_week_frgament, container, false)
        mRecyclerView = view.findViewById<View>(R.id.recyclerview_week) as RecyclerView
        mLayoutManager = LinearLayoutManager(context)
        mRecyclerView!!.layoutManager = mLayoutManager
        mRecyclerView!!.setHasFixedSize(true)
        mRecyclerView!!.itemAnimator = DefaultItemAnimator()
        getThisWeeksRecords()
        mAdapter = RecordAdapter(activity!!, this)
        mRecyclerView!!.adapter = mAdapter
        return view
    }

    private fun getThisWeeksRecords() {
        val cal = Calendar.getInstance()
        cal.set(Calendar.HOUR_OF_DAY, 0) // ! clear would not reset the hour of day !
        cal.clear(Calendar.MINUTE)
        cal.clear(Calendar.SECOND)
        cal.clear(Calendar.MILLISECOND)

        // get start of this week in milliseconds
        cal.firstDayOfWeek = Calendar.MONDAY
        cal.set(Calendar.DAY_OF_WEEK, cal.firstDayOfWeek)
        val startOfThisWeek = cal.timeInMillis

        cal.add(Calendar.DATE, 6)
        val i = cal.get(Calendar.DAY_OF_WEEK)
        cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY)
        val endOfThisWeek = cal.timeInMillis
        mainActivity!!.mRecordViewModel.getRecordsByRange(startOfThisWeek, endOfThisWeek).observe(this, Observer<List<RecordEntity>> { recordList ->
            mainActivity!!.showHideProgress(MainActivity.HIDE_PROGRESS, "")
            val mutableList = recordList as MutableList<RecordEntity>?
            if (MyPreferences.getBool(MyPreferences.IS_INCOMPLETE, false)
                    && recordList != null && recordList.isNotEmpty()) {
                recordList.removeAt(recordList!!.size - 1)
            }
            mAdapter!!.updateData(mutableList)
        })
    }

    override fun onResume() {
        super.onResume()

    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"
        // The request code must be 0 or greater.
        private val PLUS_ONE_REQUEST_CODE = 0

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment WeekFragment.
         */
        fun newInstance(param1: String, param2: String): WeekFragment {
            val fragment = WeekFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }

}// Required empty public constructor
