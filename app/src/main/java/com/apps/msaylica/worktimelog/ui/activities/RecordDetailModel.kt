package com.apps.msaylica.worktimelog.ui.activities

import android.app.Application
import android.arch.lifecycle.AndroidViewModel

internal class RecordDetailModel(application: Application) : AndroidViewModel(application)
