package com.apps.msaylica.worktimelog.ui.activities

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView

import com.apps.msaylica.worktimelog.R
import com.apps.msaylica.worktimelog.entity.RecordEntity
import com.apps.msaylica.worktimelog.viewmodel.RecordViewModel

class SettingsActivity : AppCompatActivity() {

    private var progressBar: ProgressBar? = null
    private var progressLayout: RelativeLayout? = null
    private var progressMsg: TextView? = null
    private var mRecordViewModel: RecordViewModel? = null
    private lateinit var btnClear: Button
    private var size: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        progressLayout = findViewById(R.id.progressLayout)
        mRecordViewModel = ViewModelProviders.of(this).get(RecordViewModel::class.java)
        progressBar = findViewById(R.id.indeterminateProgress)
        progressMsg = findViewById(R.id.progressMsg)
        btnClear = findViewById(R.id.btn_clear)
        btnClear.setOnClickListener{ v -> clearAllRecords()
        }
        showHideProgress(MainActivity.PROGRESS_INDETERMINATE, null)
        mRecordViewModel!!.allRecords
                .observe(this, Observer<List<RecordEntity>> {
                    if (it != null) {
                        showHideProgress(MainActivity.HIDE_PROGRESS, null)
                        size = it.size
                    }
                })
    }

    private fun clearAllRecords() {
        val dialog = AlertDialog.Builder(this).create()
        var msg:String
        if(size == 0){
            msg = getString(R.string.no_record_to_delete)
        } else {
            msg = getString(R.string.delete_all_confirmation, size)
            dialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK") { dialogInterface, i ->
                dialog.dismiss()
                mRecordViewModel!!.deleteAllRecords()
                val msg1 = getString(R.string.s_clearing_records)
                showHideProgress(MainActivity.PROGRESS_INDETERMINATE, msg1)
            }
        }
        dialog.setMessage(msg)
        dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "CANCEL") { dialogInterface, i ->
            dialog.dismiss()
            showHideProgress(MainActivity.HIDE_PROGRESS, null)
        }
        dialog.show()

    }

    override fun onBackPressed() {
        finish()
        super.onBackPressed()
    }

    private fun showHideProgress(type: Int, msg: String?) {
        when (type) {
            MainActivity.HIDE_PROGRESS -> {
                progressLayout!!.visibility = View.GONE
                progressBar!!.visibility = View.GONE
            }
            MainActivity.PROGRESS_INDETERMINATE -> {
                progressLayout!!.visibility = View.VISIBLE
                progressBar!!.visibility = View.VISIBLE
                progressMsg!!.visibility = View.VISIBLE
                progressMsg!!.text = msg
            }
        }
    }
}
