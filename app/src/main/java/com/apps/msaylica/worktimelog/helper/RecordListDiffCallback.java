package com.apps.msaylica.worktimelog.helper;

import android.support.v7.util.DiffUtil;

import com.apps.msaylica.worktimelog.entity.RecordEntity;

import java.util.List;

public class RecordListDiffCallback extends DiffUtil.Callback {
    private final List<RecordEntity> oldList, newList;

    public RecordListDiffCallback(List<RecordEntity> oldList, List<RecordEntity> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList == null ? 0 : oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList == null ? 0 : newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).getRecordId() == newList.get(newItemPosition).getRecordId();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).equals(newList.get(newItemPosition));
    }
}
