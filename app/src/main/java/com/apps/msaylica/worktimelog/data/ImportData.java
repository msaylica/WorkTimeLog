package com.apps.msaylica.worktimelog.data;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.apps.msaylica.worktimelog.R;
import com.apps.msaylica.worktimelog.entity.RecordEntity;
import com.apps.msaylica.worktimelog.ui.activities.MainActivity;
import com.apps.msaylica.worktimelog.db.vo.TimeLogRecordVo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class ImportData {

    private static final String DELIMITER = ",";
    private final Context context;
    private final MainActivity mainActivity;
    private LinearLayout progressLayout;
    private ProgressBar determinateProgressBar, indeterminateProgressBar;
    private TextView progressText;

    public ImportData(Context context) {
        this.context = context;
        mainActivity = (MainActivity) context;
    }

    public void importAllRecords(File file) {
        getTimeRecordData(file);
    }


    private void getTimeRecordData(File file) {
        mainActivity.runOnUiThread(() -> mainActivity.showHideProgress(MainActivity.PROGRESS_INDETERMINATE, mainActivity.getString(R.string.s_reading_records)));
        ArrayList<RecordEntity> recordList = new ArrayList<>();

        BufferedReader br = null;
        try {
            if (file.exists()) {
                br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            } else {
                //  br = new BufferedReader(new InputStreamReader(assetManager.open(fileName)));
            }
            br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));

            String line;
            int lineNum = 0;
            while ((line = br.readLine()) != null) {
                if (lineNum == 0) {
                    lineNum++;
                    continue;
                }

                if (!line.trim().isEmpty()) {
                    String line2 = line.replace("\"", "");
                    String[] values = line2.split(DELIMITER, -1);
                    RecordEntity record = new RecordEntity();
                    record.setRecordId(Integer.parseInt(values[0]));
                    record.setTimeIn(mainActivity.getTimeInMillis(values[1]));
                    record.setTimeOut(mainActivity.getTimeInMillis(values[2]));
                    record.setTimeBreak(Integer.parseInt(values[3]));
                    record.setLongitude(values[4]);
                    record.setLatitude(values[5]);
                    record.setDuration(values[6]);
                    record.setNotes(values[7]);
                    record.setDate(values[8]);

                    record.setAddress(extractAddress(values, values[9], 10));
                    recordList.add(record);
                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (br != null) br.close();
            } catch (IOException ex) {
                //
            }
        }
        insertRecords(recordList);
    }

    private String extractAddress(String[] values, String address, int valueIndex) {
        address = address + "," + values[valueIndex];
        if (++valueIndex < values.length){
            extractAddress(values, address, valueIndex);
        }
        return address;
    }

    private void insertRecords(ArrayList<RecordEntity> timeRecordData) {
        mainActivity.runOnUiThread(() -> {
            mainActivity.showHideProgress(MainActivity.HIDE_PROGRESS, null);
            mainActivity.showHideProgress(MainActivity.PROGRESS_DETERMINATE, mainActivity.getString(R.string.s_importing));
        });

        int position = 0;
        for (RecordEntity recordEntity : timeRecordData) {
            position++;
            final int progress = (int) ((position / (float) timeRecordData.size()) * 100);
            mainActivity.runOnUiThread(() -> mainActivity.updateProgress(progress));
            mainActivity.mRecordViewModel.insertRecord(recordEntity);
        }
        mainActivity.importCompleted(timeRecordData.size());
    }
}
