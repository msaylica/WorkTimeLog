package com.apps.msaylica.worktimelog.ui.activities

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.widget.ProgressBar
import android.widget.Toast

import com.apps.msaylica.worktimelog.R

class ImportExportActivity : AppCompatActivity() {
    private val progressBar: ProgressBar? = null
    private var exportType: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_import_export)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        //        progressBar = new ProgressBar(this);
        //        progressBar.findViewById(R.id.importProgress);
        if (intent.extras != null) {
            exportType = intent.getIntExtra(MainActivity.EXPORT_TYPE, MainActivity.LOCAL_EXPORT)
        }
    }

    private fun requestWriteToStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            ActivityCompat.requestPermissions(this@ImportExportActivity,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    REQUEST_WRITE_PERMISSION)
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    REQUEST_WRITE_PERMISSION)
        }
    }

    private fun browseForFile() {
        val fileIntent = Intent(Intent.ACTION_GET_CONTENT)
        fileIntent.type = "*/*"
        if (fileIntent.resolveActivity(packageManager) != null) {
            startActivityForResult(fileIntent, BROWSE_CSV_FILE)
        } else {
            Toast.makeText(this, "You do not have an app to perform file browsing.", Toast.LENGTH_LONG).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        // Check which request we're responding to
        if (requestCode == BROWSE_CSV_FILE) {
            if (resultCode == Activity.RESULT_OK) {
                val _uri = data.data
                Log.d("", "URI = " + _uri!!)
                if (_uri != null) {
                    if (!_uri.path!!.contains(".csv")) {
                        val msg = "Please select a file in CSV format."
                        val title = "Error!"
                        showAlertDialog(msg, title)
                    } else {
                        //      progressBar.setVisibility(View.VISIBLE);
                        //                        filePath = _uri.getPath().replace("/document/raw:", "");
                        //                        File file = new File(filePath);
                        //                        ArrayList<TimeLogRecordVo> timeRecordData = ImportData.getTimeRecordData(this, file);
                        //                        TimeLogRecordDao dao = new TimeLogRecordDao(null, this);
                        //                        dao.open();
                        //                        for (TimeLogRecordVo vo : timeRecordData) {
                        //                            dao.insertRecord(vo);
                        //                        }
                        //                        //    progressBar.setVisibility(View.GONE);
                        //                        Intent returnIntent = new Intent();
                        //                        returnIntent.putExtra(MainActivity.NUM_RECORDS_IMPORTED, timeRecordData.size());
                        //                        setResult(Activity.RESULT_OK, returnIntent);
                        //                        finish();
                    }
                }
            }
        }
    }

    private fun showAlertDialog(msg: String, title: String) {
        val alertDialog = AlertDialog.Builder(this).create()
        alertDialog.setTitle(title)
        alertDialog.setMessage(msg)
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK"
        ) { dialog, which -> dialog.dismiss() }
        alertDialog.show()
    }

    companion object {

        private val BROWSE_CSV_FILE = 1
        // Used when request read external storage permission.
        private val REQUEST_PERMISSION_READ_EXTERNAL = 2
        private val REQUEST_WRITE_PERMISSION = 3
    }


}
