package com.apps.msaylica.worktimelog.helper

import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale

object TimeHelper {
    fun getFormatedTime(durationInMillis: Long): String {

        val calendar = Calendar.getInstance()
        calendar.time = Date(durationInMillis)
        val simpleDateFormatformat = SimpleDateFormat("h:mm a")
        return simpleDateFormatformat.format(calendar.time)
    }

    fun getTimeDiff(durationInMillis: Long): Double {

        val calendar = Calendar.getInstance()
        calendar.time = Date(durationInMillis)


        val seconds = (durationInMillis / 1000).toInt() % 60
        var minutes = (durationInMillis / (1000 * 60) % 60).toInt()
        val hours = (durationInMillis / (1000 * 60 * 60) % 24).toInt()

        val fSeconds = seconds.toString()

        if (seconds > 30) {
            minutes++
        }

        val realMinute = minutes / 60.0

        // formattedDate have current date/time
        return hours + realMinute
    }

    fun round(value: Double, places: Int): Double {
        var value = value
        if (places < 0) throw IllegalArgumentException()

        val factor = Math.pow(10.0, places.toDouble()).toLong()
        value = value * factor
        val tmp = Math.round(value)
        return tmp.toDouble() / factor
    }

    fun getDate(durationInMillis: Long): String {

        val calendar = Calendar.getInstance()
        calendar.time = Date(durationInMillis)
        val simpleDateFormatformat = SimpleDateFormat("EEE d MMM yyyy")
        return simpleDateFormatformat.format(calendar.time)
    }

    fun getDay(timeInMillis: Long): String? {
        val calendar = Calendar.getInstance()
        val toDay = SimpleDateFormat("EEEE", Locale.ENGLISH).format(calendar.time)
        calendar.time = Date(timeInMillis)
        var recordDay = SimpleDateFormat("EEEE", Locale.ENGLISH).format(calendar.time)
        if (toDay == recordDay)
            recordDay = "Today"
        return recordDay
    }

}
