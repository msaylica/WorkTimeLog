package com.apps.msaylica.worktimelog

import android.app.Application
import android.arch.lifecycle.LiveData
import android.database.Cursor
import android.os.AsyncTask

import com.apps.msaylica.worktimelog.db.RecordDao
import com.apps.msaylica.worktimelog.db.RecordRoomDatabase
import com.apps.msaylica.worktimelog.entity.RecordEntity

class RecordRepository(application: Application) {
    private val mRecordDao: RecordDao
    private val mAllRecords: LiveData<List<RecordEntity>>
    private val executor: AppExecutors = AppExecutors()

    val latestRecord: LiveData<RecordEntity>
        get() = mRecordDao.latestRecord

    val allRecords: LiveData<List<RecordEntity>>
        get() = mRecordDao.allRecords

    val cursorAll: Cursor
        get() = mRecordDao.cursorAll

    init {
        val db = RecordRoomDatabase.getInstance(application, executor)
        mRecordDao = db.recordDao()
        mAllRecords = mRecordDao.allRecords
    }


    internal fun getmAllRecords(): LiveData<List<RecordEntity>> {
        return mAllRecords
    }

    fun insert(record: RecordEntity) {
        InsertAsyncTask(mRecordDao).execute(record)
    }

    fun getRecordsByRange(start: Long, end: Long): LiveData<List<RecordEntity>> {
        return mRecordDao.getRecordByRange(start.toDouble(), end.toDouble())
    }

    fun getRecordById(id: Int): RecordEntity {
        return mRecordDao.getRecordById(id)
    }

    fun updateRecord(record: RecordEntity) {
        UpdateAsyncTask(mRecordDao).execute(record)
    }

    fun getTodaysRecords(todayInMillis: Long, nextDayInMillis: Long): LiveData<MutableList<RecordEntity>> {
        return mRecordDao.getRecordByRange(todayInMillis.toDouble(), nextDayInMillis.toDouble())
    }

    fun deleteRecord(record: RecordEntity) {
        DeleteAsyncTask(mRecordDao).execute(record)
    }

    fun getRecordByIdLive(recordId: Int): LiveData<RecordEntity> {
        return mRecordDao.getRecordByIdLive(recordId)
    }

    fun deleteAllRecords() {
        DeleteAllTask(mRecordDao).execute()
    }

    private class InsertAsyncTask internal constructor(private val mAsyncTaskDao: RecordDao) : AsyncTask<RecordEntity, Void, Void>() {

        override fun doInBackground(vararg params: RecordEntity): Void? {
            mAsyncTaskDao.insert(params[0])
            return null
        }
    }


    private class UpdateAsyncTask constructor(private val mAsyncTaskDao: RecordDao) : AsyncTask<RecordEntity, Void, Void>() {

        override fun doInBackground(vararg params: RecordEntity): Void? {
            mAsyncTaskDao.update(params[0])
            return null
        }
    }

    private class DeleteAsyncTask constructor(private val mAsyncTaskDao: RecordDao) : AsyncTask<RecordEntity, Void, Void>() {

        override fun doInBackground(vararg recordEntities: RecordEntity): Void? {
            mAsyncTaskDao.delete(recordEntities[0])
            return null
        }
    }

    private class DeleteAllTask constructor(private val mAyncTaskDoa: RecordDao) : AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg voids: Void): Void? {
            mAyncTaskDoa.deleteAll()
            return null
        }
    }
}
