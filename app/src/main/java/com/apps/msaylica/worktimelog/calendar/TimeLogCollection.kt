package com.apps.msaylica.worktimelog.calendar

import java.text.SimpleDateFormat
import java.util.ArrayList
import java.util.Calendar
import java.util.Date
import java.util.Locale

class TimeLogCollection(var id: Int, var timeIn: Long, var timeOut: Long, var timeBreak: Int,
                        var longitude: String, var latitude: String, var address: String, var notes: String, var duration: String) {
    var date: String

    init {
        this.date = getDate(timeIn)
    }

    fun getDate(durationInMillis: Long): String {

        val calendar = Calendar.getInstance()
        calendar.time = Date(durationInMillis)
        val simpleDateFormatformat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        return simpleDateFormatformat.format(calendar.time)
    }

    companion object {
        var date_collection_arr: ArrayList<TimeLogCollection>? = null
    }
}
