package com.apps.msaylica.worktimelog.ui.fragments

import android.app.DatePickerDialog
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.arch.lifecycle.Observer

import com.apps.msaylica.worktimelog.R
import com.apps.msaylica.worktimelog.ui.activities.MainActivity
import com.apps.msaylica.worktimelog.ui.adapter.RecordAdapter2
import com.apps.msaylica.worktimelog.entity.RecordEntity
import com.apps.msaylica.worktimelog.prefs.MyPreferences
import java.util.*

/**
 * A fragment with a Google +1 button.
 * Activities that contain this fragment must implement the
 * [MonthFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [MonthFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MonthFragment : Fragment() {
    // The URL to +1.  Must be a valid URL.
    private val PLUS_ONE_URL = "http://developer.android.com"
    private var mParam1: String? = null
    private var mParam2: String? = null

    private var mListener: OnFragmentInteractionListener? = null
    private var txtStartDate: TextView? = null
    private var txtEndDate: TextView? = null
    private var mainActivity: MainActivity? = null
    private var mRecyclerView: RecyclerView? = null
    private var mLayoutManager: LinearLayoutManager? = null
    private var mAdapter: RecordAdapter2? = null
    private val customPeriodRecords: MutableList<RecordEntity>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.mainActivity = activity as MainActivity?
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_month, container, false)
        txtStartDate = view.findViewById(R.id.txt_start_date)
        txtEndDate = view.findViewById(R.id.txt_end_date)
        txtStartDate!!.setOnClickListener { view12 -> setStartDate() }
        txtEndDate!!.setOnClickListener { view1 -> setEndDate() }

        mRecyclerView = view.findViewById(R.id.recyclerview_custom_period)
        mLayoutManager = LinearLayoutManager(context)
        mRecyclerView!!.layoutManager = mLayoutManager
        val startDate = MyPreferences.getLong(MyPreferences.CUSTOM_START_DATE, 0)
        val endDate = MyPreferences.getLong(MyPreferences.CUSTOM_END_DATE, 0)
        setDates(startDate, endDate)
        return view
    }

    private fun setDates(startDate: Long, endDate: Long) {
        var startDate = startDate
        var endDate = endDate
        val cal = Calendar.getInstance()
        if (startDate != 0L) {
            cal.timeInMillis = startDate
        } else {
            cal.set(Calendar.DAY_OF_MONTH, 1)
            startDate = cal.timeInMillis
        }
        var year = cal.get(Calendar.YEAR)
        var month = cal.get(Calendar.MONTH) + 1
        var day = cal.get(Calendar.DAY_OF_MONTH)
        val startDateStr = "$month/$day/$year"
        txtStartDate!!.text = startDateStr
        if (endDate != 0L) {
            cal.timeInMillis = endDate
        } else {
            cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH))
            endDate = cal.timeInMillis
        }
        year = cal.get(Calendar.YEAR)
        month = cal.get(Calendar.MONTH) + 1
        day = cal.get(Calendar.DAY_OF_MONTH)
        val endDateStr = "$month/$day/$year"
        txtEndDate!!.text = endDateStr
        mAdapter = RecordAdapter2(this.activity!!)
        mRecyclerView!!.adapter = mAdapter
        getRecords(startDate, endDate)
    }

    private fun getRecords(startDate: Long, endDate: Long) {

        mainActivity!!.showHideProgress(MainActivity.PROGRESS_INDETERMINATE, resources.getString(R.string.please_wait))
        mainActivity!!.mRecordViewModel.getRecordsByRange(startDate, endDate).observe(this, Observer<List<RecordEntity>> { recordList ->
            mainActivity!!.showHideProgress(MainActivity.HIDE_PROGRESS, "")
            mAdapter!!.updateData(recordList as MutableList<RecordEntity>?)
        })

    }

    private fun setEndDate() {
        val mcurrentTime = Calendar.getInstance()
        val year = mcurrentTime.get(Calendar.YEAR)
        val month = mcurrentTime.get(Calendar.MONTH)
        val day = mcurrentTime.get(Calendar.DAY_OF_MONTH)
        val dlg = DatePickerDialog(context!!, { datePicker, i, i1, i2 ->
            mcurrentTime.set(i, i1, i2)
            val timeInMillis = mcurrentTime.timeInMillis
            val date = "$i1/$i2/$i"
            txtEndDate!!.text = date
            MyPreferences.setLong(MyPreferences.CUSTOM_END_DATE, timeInMillis)
            getRecords(MyPreferences.getLong(MyPreferences.CUSTOM_START_DATE, 0), timeInMillis)
        }, year, month, day)

        dlg.show()
    }

    private fun setStartDate() {
        val mcurrentTime = Calendar.getInstance()
        val year = mcurrentTime.get(Calendar.YEAR)
        val month = mcurrentTime.get(Calendar.MONTH)
        val day = mcurrentTime.get(Calendar.DAY_OF_MONTH)
        val dlg = DatePickerDialog(context!!, { datePicker, i, i1, i2 ->
            mcurrentTime.set(i, i1, i2)
            val timeInMillis = mcurrentTime.timeInMillis
            val date = "$i1/$i2/$i"
            txtStartDate!!.text = date
            MyPreferences.setLong(MyPreferences.CUSTOM_START_DATE, timeInMillis)
            getRecords(timeInMillis, MyPreferences.getLong(MyPreferences.CUSTOM_END_DATE, 0))
        }, year, month, day)

        dlg.show()
    }

    override fun onResume() {
        super.onResume()

    }

    fun onButtonPressed(uri: Uri) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(uri)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    fun createdNewCustomRecord(record: RecordEntity) {
        val adapter = mAdapter
        customPeriodRecords!!.add(0, record)
        adapter!!.notifyItemChanged(0)
        // adapter.focusOnNewRecord(mRecyclerView);
    }

    companion object {
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"
        // The request code must be 0 or greater.
        private val PLUS_ONE_REQUEST_CODE = 0

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment MonthFragment.
         */
        fun newInstance(param1: String, param2: String): MonthFragment {
            val fragment = MonthFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }

}// Required empty public constructor
