package com.apps.msaylica.worktimelog.ui.adapter

import android.app.Activity
import android.content.Intent
import android.support.v7.util.DiffUtil
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.apps.msaylica.worktimelog.R
import com.apps.msaylica.worktimelog.entity.RecordEntity
import com.apps.msaylica.worktimelog.helper.RecordListDiffCallback
import com.apps.msaylica.worktimelog.ui.activities.MainActivity
import com.apps.msaylica.worktimelog.ui.activities.RecordDetailActivity

import java.util.ArrayList
import java.util.concurrent.TimeUnit

class RecordAdapter2(activity: Activity) : RecyclerView.Adapter<RecordAdapter2.ViewHolder>() {

    private var dataset: MutableList<RecordEntity>? = null
    val activity: MainActivity


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal val txtDayMonth: TextView = itemView.findViewById(R.id.txt_day_month)
        internal val txtDay: TextView = itemView.findViewById(R.id.txt_day)
        internal val txtShift: TextView = itemView.findViewById(R.id.txtShift)
        internal val txtBreak: TextView = itemView.findViewById(R.id.txt_break)
        internal val txtHoursWorked: TextView = itemView.findViewById(R.id.txt_hours_worked)
        internal val cardView: CardView = itemView.findViewById(R.id.item_card_view)
        internal val mToolbar: Toolbar = itemView.findViewById(R.id.record_toolbar)

    }

    init {
        this.dataset = ArrayList()
        this.activity = activity as MainActivity

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val cardView = LayoutInflater.from(parent.context).inflate(R.layout.item_record3, parent, false) as CardView
        val v = ViewHolder(cardView)
        v.mToolbar.inflateMenu(R.menu.menu_record_item)
        return v
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val record = dataset!![position]
        holder.mToolbar.setOnMenuItemClickListener { item ->

            when (item.itemId) {
                R.id.action_edit -> showDetails(dataset!![position])
                R.id.action_duplicate -> {
                }
                R.id.action_delete -> activity.mRecordViewModel.deleteRecord(record)
            }
            false
        }
        val dayMonth = activity.getDayMonth(record.timeIn)
        holder.cardView.setOnClickListener { view -> showDetails(record) }
        holder.txtDayMonth.text = dayMonth
        holder.txtDay.text = activity.getDayShort(record.timeIn)

        val sb = StringBuilder()
        sb.append(activity.getFormatedTime(record.timeIn))
        sb.append(" - ")
        sb.append(activity.getFormatedTime(record.timeOut))
        holder.txtShift.text = sb
        holder.txtBreak.text = String.format("%d", record.timeBreak)
        val breakInMillis = TimeUnit.MINUTES.toMillis(record.timeBreak.toLong())
        val durationInMillis = record.timeOut - record.timeIn
        activity.getTimeDiff(durationInMillis)
        val duration = MainActivity.round(activity.getTimeDiff(durationInMillis - breakInMillis), 2).toString() + " hours"
        holder.txtHoursWorked.text = duration
    }

    private fun showDetails(dataset: RecordEntity) {
        val intent = Intent(activity, RecordDetailActivity::class.java)
        intent.putExtra("RecordId", dataset.recordId)
        activity.startActivityForResult(intent, MainActivity.RECORD_DETAIL)
    }


    override fun getItemCount(): Int {
        return if (dataset == null) 0 else dataset!!.size
    }

    fun updateData(recordList: MutableList<RecordEntity>?) {
        if (recordList != null) {
            val recordListDiffCallback = RecordListDiffCallback(dataset, recordList)
            val diffResult = DiffUtil.calculateDiff(recordListDiffCallback)

            dataset!!.clear()
            dataset!!.addAll(recordList)
            diffResult.dispatchUpdatesTo(this)
        } else {
            dataset = recordList
            notifyDataSetChanged()
        }
    }
}
