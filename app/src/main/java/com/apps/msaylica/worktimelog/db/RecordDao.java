package com.apps.msaylica.worktimelog.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import android.database.Cursor;

import com.apps.msaylica.worktimelog.entity.RecordEntity;

import java.util.List;


@Dao
public interface RecordDao {

    @Insert
    void insert(RecordEntity record);

    @Query("DELETE FROM record_table")
    void deleteAll();

    @Query("SELECT * FROM record_table ORDER BY recordId ASC")
    LiveData<List<RecordEntity>> getAllRecords();

    @Query("SELECT * FROM record_table ORDER BY recordId DESC LIMIT 1")
    LiveData<RecordEntity> getLatestRecord();

    @Query("SELECT * FROM record_table where recordId = :id")
    RecordEntity getRecordById(int id);

    @Query("select * from record_table where timeIn = :timeIn")
    LiveData<RecordEntity> getRecordByTimeIn(double timeIn);

    @Query("select * from record_table where timeIn between :start and :end ORDER BY recordId DESC")
    LiveData<List<RecordEntity>> getRecordByRange(double start, double end);

    @Update
    void update(RecordEntity record);

    @Delete
    void delete(RecordEntity record);

    @Query("select * from record_table where recordId = :recordId")
    LiveData<RecordEntity> getRecordByIdLive(int recordId);

    @Query("select * from record_table")
    Cursor getCursorAll();
}
