package com.apps.msaylica.worktimelog.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "record_table")
class RecordEntity {
    @PrimaryKey(autoGenerate = true)
    var recordId: Int = 0
    var timeIn: Long = 0
    var timeOut: Long = 0
    var timeBreak: Int = 0
    var longitude: String? = null
    var latitude: String? = null
    var notes: String? = null
    var duration: String? = null
    var date: String? = null
    var address: String? = null

}
