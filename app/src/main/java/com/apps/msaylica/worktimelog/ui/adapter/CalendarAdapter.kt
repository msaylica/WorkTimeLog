package com.apps.msaylica.worktimelog.ui.adapter

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ListView
import android.widget.TextView
import com.apps.msaylica.worktimelog.R
import com.apps.msaylica.worktimelog.calendar.Recordpojo
import com.apps.msaylica.worktimelog.calendar.TimeLogCollection
import com.apps.msaylica.worktimelog.entity.RecordEntity
import com.apps.msaylica.worktimelog.ui.activities.MainActivity
import kotlinx.android.synthetic.main.dialog_inform.view.*
import org.json.JSONArray
import org.json.JSONException
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.ArrayList
import java.util.Calendar
import java.util.Locale

class CalendarAdapter(private val context: Context, monthCalendar: Calendar, var date_collection_arr: ArrayList<TimeLogCollection>) : BaseAdapter() {
    lateinit var mAlertDialog: AlertDialog
    private val mainActvity: MainActivity

    private val month: java.util.Calendar
    lateinit var pmonth: Calendar
    /**
     * calendar instance for previous month for getting complete view
     */
    lateinit var pmonthmaxset: Calendar
    private val selectedDate: Calendar
    private var firstDay: Int = 0
    private var maxWeekNumber: Int = 0
    private var maxP: Int = 0
    private var calMaxP: Int = 0
    private var mnthlength: Int = 0
    private lateinit var itemvalue: String
    private var currentDateString: String
    private var df: DateFormat

    private val items: ArrayList<String>
    private var gridvalue: String? = null
    private var listRecords: ListView? = null
    private var alCustom = ArrayList<Recordpojo>()
    private var btnAddRecord: TextView? = null
    private var txtInfo: TextView? = null

    init {
        CalendarAdapter.day_string = ArrayList()
        Locale.setDefault(Locale.US)
        month = monthCalendar
        selectedDate = monthCalendar.clone() as Calendar
        mainActvity = context as MainActivity
        month.set(Calendar.DAY_OF_MONTH, 1)
        this.items = ArrayList()
        df = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        currentDateString = df.format(selectedDate.time)
        refreshDays()

    }

    override fun getCount(): Int {
        return day_string.size
    }

    override fun getItem(position: Int): Any {
        return day_string[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    // create a new view for each item referenced by the Adapter
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var v = convertView
        val dayView: TextView
        if (convertView == null) { // if it's not recycled, initialize some
            // attributes
            val vi = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            v = vi.inflate(R.layout.cal_item, null)

        }


        dayView = v!!.findViewById<View>(R.id.date) as TextView
        val separatedTime = day_string[position].split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()


        gridvalue = separatedTime[2].replaceFirst("^0*".toRegex(), "")
        if (Integer.parseInt(gridvalue) > 1 && position < firstDay) {
            dayView.setTextColor(Color.parseColor("#A9A9A9"))
            dayView.isClickable = false
            dayView.isFocusable = false
        } else if (Integer.parseInt(gridvalue) < 7 && position > 28) {
            dayView.setTextColor(Color.parseColor("#A9A9A9"))
            dayView.isClickable = false
            dayView.isFocusable = false
        } else {
            // setting curent month's days in blue color.
            dayView.setTextColor(Color.parseColor("#696969"))
        }


        if (day_string[position] == currentDateString) {

            v.setBackgroundColor(Color.parseColor("#ffffff"))
        } else {
            v.setBackgroundColor(Color.parseColor("#ffffff"))
        }


        dayView.text = gridvalue

        // create date string for comparison
        var date = day_string[position]

        if (date.length == 1) {
            date = "0$date"
        }
        var monthStr = "" + (month.get(Calendar.MONTH) + 1)
        if (monthStr.length == 1) {
            monthStr = "0$monthStr"
        }

        setEventView(v, position, dayView)

        return v
    }

    fun refreshDays() {
        // clear items
        items.clear()
        day_string.clear()
        Locale.setDefault(Locale.US)
        pmonth = month.clone() as Calendar
        // month start day. ie; sun, mon, etc
        firstDay = month.get(Calendar.DAY_OF_WEEK)
        // finding number of weeks in current month.
        maxWeekNumber = month.getActualMaximum(Calendar.WEEK_OF_MONTH)
        // allocating maximum row number for the gridview.
        mnthlength = maxWeekNumber * 7
        maxP = getMaxP() // previous month maximum day 31,30....
        calMaxP = maxP - (firstDay - 1)// calendar offday starting 24,25 ...
        pmonthmaxset = pmonth.clone() as Calendar

        pmonthmaxset.set(Calendar.DAY_OF_MONTH, calMaxP + 1)


        for (n in 0 until mnthlength) {

            itemvalue = df.format(pmonthmaxset.time)
            pmonthmaxset.add(Calendar.DATE, 1)
            day_string.add(itemvalue)

        }
    }

    private fun getMaxP(): Int {
        val maxP: Int
        if (month.get(Calendar.MONTH) == month
                        .getActualMinimum(Calendar.MONTH)) {
            pmonth.set(month.get(Calendar.YEAR) - 1,
                    month.getActualMaximum(Calendar.MONTH), 1)
        } else {
            pmonth.set(Calendar.MONTH,
                    month.get(Calendar.MONTH) - 1)
        }
        maxP = pmonth.getActualMaximum(Calendar.DAY_OF_MONTH)

        return maxP
    }


    fun setEventView(v: View, pos: Int, txt: TextView) {

        val len = TimeLogCollection.date_collection_arr!!.size
        for (i in 0 until len) {
            val cal_obj = TimeLogCollection.date_collection_arr!![i]
            val date = cal_obj.date
            val len1 = day_string.size
            if (len1 > pos) {

                if (day_string[pos] == date) {
                    if (Integer.parseInt(gridvalue) > 1 && pos < firstDay) {

                    } else if (Integer.parseInt(gridvalue) < 7 && pos > 28) {

                    } else {
                        v.setBackgroundColor(Color.parseColor("#ffb74d"))
                        v.setBackgroundResource(R.drawable.rounded_calender)
                        txt.setTextColor(Color.parseColor("#696969"))
                    }

                }
            }
        }
    }

    fun getPositionList(date: String, act: Activity) {

        val len = TimeLogCollection.date_collection_arr!!.size
        val recordList = ArrayList<RecordEntity>()

        for (j in 0 until len) {
            if (TimeLogCollection.date_collection_arr!![j].date == date) {
                val record = RecordEntity()
                record.recordId = TimeLogCollection.date_collection_arr!![j].id
                record.date = TimeLogCollection.date_collection_arr!![j].date
                record.timeIn = TimeLogCollection.date_collection_arr!![j].timeIn
                record.timeOut = TimeLogCollection.date_collection_arr!![j].timeOut
                record.timeBreak = TimeLogCollection.date_collection_arr!![j].timeBreak
                record.longitude = TimeLogCollection.date_collection_arr!![j].longitude
                record.latitude = TimeLogCollection.date_collection_arr!![j].latitude
                record.address = TimeLogCollection.date_collection_arr!![j].address
                record.notes = TimeLogCollection.date_collection_arr!![j].notes
                record.duration = TimeLogCollection.date_collection_arr!![j].duration
                recordList.add(record)
            }
        }

        //Inflate the dialog with custom view
        val mDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_inform, null)
        // val dialogTitle = if(recordList.size>1) "Records" else "Record"
        val mBuilder = AlertDialog.Builder(context)
                .setView(mDialogView)
//                .setTitle(dialogTitle)
        //show dialog
        mAlertDialog = mBuilder.show()
        val txtNoRecord = mDialogView.tv_no_record
        val btnAddRecord = mDialogView.btn_add_record
        val listRecords = mDialogView.list_records

        if (recordList.size != 0) {
            txtNoRecord.visibility = View.GONE
            btnAddRecord.visibility = View.GONE
        } else {
            txtNoRecord.visibility = View.VISIBLE
            btnAddRecord.visibility = View.VISIBLE
        }
        listRecords.layoutManager = LinearLayoutManager(context)
        listRecords.adapter = DialogAdaptorRecord(this, context, recordList)
        mDialogView.img_cross.setOnClickListener {
            mAlertDialog.dismiss()
        }

        btnAddRecord.setOnClickListener {
            addRecord(date)
        }
    }

    private fun addRecord(dateString: String) {
        mainActvity.isCustomRecord = true
        val sdf = SimpleDateFormat("yyyy-M-dd")
        val timeInMillis: Long
        try {
            val date = sdf.parse(dateString)
            val calendar = Calendar.getInstance()
            calendar.firstDayOfWeek = Calendar.MONDAY
            calendar.time = date
            timeInMillis = calendar.timeInMillis
            println("Given Time in milliseconds : " + calendar.timeInMillis)
        } catch (e: ParseException) {
            e.printStackTrace()
            return
        }

        val record = RecordEntity()
        record.timeIn = timeInMillis
        record.timeOut = timeInMillis
        record.timeBreak = 0
        record.latitude = ""
        record.longitude = ""
        record.address = ""//Specifying the pattern of input date and time
        record.duration = ""
        record.notes = ""
        mainActvity.mRecordViewModel.insertRecord(record)
    }

    private fun getMatchList(detail: String): ArrayList<Recordpojo> {
        try {
            val jsonArray = JSONArray(detail)
            alCustom = ArrayList()
            for (i in 0 until jsonArray.length()) {

                val jsonObject = jsonArray.optJSONObject(i)

                val pojo = Recordpojo()
                pojo.setId(jsonObject.optInt("id"))
                pojo.date = jsonObject.optString("date")
                pojo.timeIn = jsonObject.optString("timeIn")
                pojo.timeOut = jsonObject.optString("timeOut")
                pojo.timeBreak = jsonObject.optString("timeBreak")
                pojo.setLongitude(jsonObject.optLong("longitude"))
                pojo.setLatitude(jsonObject.optLong("latitude"))
                pojo.address = jsonObject.optString("address")
                pojo.note = jsonObject.optString("note")
                pojo.duration = jsonObject.optString("duration")
                alCustom.add(pojo)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        return alCustom
    }

    fun dismissDialog() {
        mAlertDialog.dismiss()
    }

    companion object {
        lateinit var day_string: MutableList<String>
    }
}

