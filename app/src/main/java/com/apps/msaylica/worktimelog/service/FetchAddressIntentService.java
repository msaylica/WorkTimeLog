package com.apps.msaylica.worktimelog.service;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.apps.msaylica.worktimelog.R;
import com.apps.msaylica.worktimelog.ui.activities.MainActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.content.ContentValues.TAG;

public class FetchAddressIntentService extends IntentService {
    protected ResultReceiver mReceiver;
    private Location location;

    public FetchAddressIntentService(String name) {
        super(name);
    }

    public FetchAddressIntentService() {
        super("FetchAddress");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String locationName = null;
        if (intent != null) {
            mReceiver = intent.getParcelableExtra(MainActivity.RECEIVER);
            // Get the location passed to this service through an extra.
            location = intent.getParcelableExtra(
                    MainActivity.LOCATION_DATA_EXTRA);

            locationName = intent.getExtras().getString(MainActivity.Companion.getLOCATION_NAME(), "");
        }
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        String errorMessage = "";

        List<Address> addresses = null;
        if (null != locationName && !locationName.isEmpty()) {
            try {
                addresses = geocoder.getFromLocationName(locationName, 1);
            } catch (IOException ioException) {
                // Catch network or other I/O problems.
                errorMessage = getString(R.string.service_not_available);
                Log.e(TAG, errorMessage, ioException);
            } catch (IllegalArgumentException illegalArgumentException) {
                // Catch invalid latitude or longitude values.
                errorMessage = getString(R.string.invalid_location_name);
                Log.e(TAG, errorMessage + ". " +
                        "Location Name = " + locationName + illegalArgumentException);
            }
        } else {
            try {
                addresses = geocoder.getFromLocation(
                        location.getLatitude(),
                        location.getLongitude(),
                        // In this sample, get just a single address.
                        1);

            } catch (IOException ioException) {
                // Catch network or other I/O problems.
                errorMessage = getString(R.string.service_not_available);
                Log.e(TAG, errorMessage, ioException);
            } catch (IllegalArgumentException illegalArgumentException) {
                // Catch invalid latitude or longitude values.
                errorMessage = getString(R.string.invalid_lat_long_used);
                Log.e(TAG, errorMessage + ". " +
                        "Latitude = " + location.getLatitude() +
                        ", Longitude = " +
                        location.getLongitude(), illegalArgumentException);
            }
        }


        // Handle case where no address was found.
        if (addresses == null || addresses.size() == 0) {
            if (errorMessage.isEmpty()) {
                errorMessage = getString(R.string.no_address_found);
                Log.e(TAG, errorMessage);
            }
            deliverResultToReceiver(MainActivity.FAILURE_RESULT, errorMessage);
        } else {
            Address address = addresses.get(0);
            ArrayList<String> addressFragments = new ArrayList<>();

            if (null != locationName && !locationName.isEmpty()) {
                for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                    String coordinates = "Latitude: " + address.getLatitude() + "," + " Longitude: " + address.getLongitude();
                    addressFragments.add(coordinates);
                }

            } else
                // Fetch the address lines using getAddressLine,
                // join them, and send them to the thread.
                for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                    addressFragments.add(address.getAddressLine(i));
                }
            Log.i(TAG, getString(R.string.address_found));
            deliverResultToReceiver(MainActivity.SUCCESS_RESULT,
                    TextUtils.join(System.getProperty("line.separator"),
                            addressFragments));
        }
    }

    private void deliverResultToReceiver(int resultCode, String message) {
        Bundle bundle = new Bundle();
        bundle.putString(MainActivity.RESULT_DATA_KEY, message);
        mReceiver.send(resultCode, bundle);
    }
}
