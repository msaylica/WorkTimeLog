package com.apps.msaylica.worktimelog.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.database.Cursor

import com.apps.msaylica.worktimelog.RecordRepository
import com.apps.msaylica.worktimelog.entity.RecordEntity

class RecordViewModel(application: Application) : AndroidViewModel(application) {

    private val mRepository: RecordRepository

    private val mRecords: LiveData<List<RecordEntity>>? = null

    private val mRecord: LiveData<RecordEntity>? = null

    val latestRecord: LiveData<RecordEntity>
        get() = mRepository.latestRecord

    val allRecords: LiveData<List<RecordEntity>>
        get() = mRepository.allRecords

    val cursorAll: Cursor
        get() = mRepository.cursorAll

    init {
        mRepository = RecordRepository(application)
    }

    fun getRecordsByRange(start: Long, end: Long): LiveData<List<RecordEntity>> {
        return mRepository.getRecordsByRange(start, end)
    }

    fun getRecordById(id: Int): RecordEntity {
        return mRepository.getRecordById(id)
    }

    fun updateRecord(record: RecordEntity) {
        mRepository.updateRecord(record)
    }

    fun getTodaysRecords(todayInMillis: Long, nextDayInMillis: Long): LiveData<MutableList<RecordEntity>> {
        return mRepository.getTodaysRecords(todayInMillis, nextDayInMillis)
    }

    fun insertRecord(record: RecordEntity) {
        mRepository.insert(record)
    }

    //    public void updateLastRecord(long timeInMillis) {
    //        mRepository.updateLastRecord(timeInMillis);
    //    }

    fun deleteRecord(record: RecordEntity) {
        mRepository.deleteRecord(record)
    }

    fun getRecordByIdLive(recordId: Int): LiveData<RecordEntity> {
        return mRepository.getRecordByIdLive(recordId)
    }

    fun deleteAllRecords() {
        mRepository.deleteAllRecords()
    }
}
