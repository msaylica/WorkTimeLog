package com.apps.msaylica.worktimelog.calendar

class Recordpojo {
    var date: String? = null
    var timeIn: String? = null
    var timeOut: String? = null
    var timeBreak: String? = null
    var address: String? = null
    var note: String? = null
    var duration: String? = null
    private var id: Int = 0
    private var longitude: Long = 0
    private var latitude: Long = 0

    fun setId(id: Int) {
        this.id = id
    }

    fun setLongitude(longitude: Long) {
        this.longitude = longitude
    }

    fun setLatitude(latitude: Long) {
        this.latitude = latitude
    }
}