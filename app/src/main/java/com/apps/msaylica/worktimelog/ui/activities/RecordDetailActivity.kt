package com.apps.msaylica.worktimelog.ui.activities

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.arch.lifecycle.Observer
import android.content.Context
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v7.app.ActionBar
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView

import android.widget.Toast

import com.apps.msaylica.worktimelog.R
import com.apps.msaylica.worktimelog.entity.RecordEntity
import com.apps.msaylica.worktimelog.prefs.MyPreferences
import com.apps.msaylica.worktimelog.ui.fragments.AddBreakDialogFragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapFragment
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions

import java.util.Calendar
import java.util.Objects
import java.util.concurrent.TimeUnit

class RecordDetailActivity : MainActivity(), OnMapReadyCallback, AddBreakDialogFragment.AddBreakDialogListener {

    internal lateinit var googleMap: GoogleMap
    internal lateinit var mapFragment: MapFragment
    private var timePickerTitle: String? = null
    private var fullDate: TextView? = null
    private var timeIn: TextView? = null
    private var timeOut: TextView? = null
    private var txtBreakTime: TextView? = null
    private var note: TextView? = null
    private var hoursWorked: TextView? = null
    private var alertDialog: AlertDialog? = null
    private var locationText: TextView? = null
    private var locationManager: LocationManager? = null
    private var locationLayout: LinearLayout? = null
    private var btnAddLocation: TextView? = null
    private var recordMapMarker: Marker? = null
    private var recordId: Int = 0
    private var record: RecordEntity? = null
    private var initialLocationRequest: Boolean = false
    private var displayedAddress: String? = null

    private var editing: Editing? = null

    private// Found best last known location: %s", l);
    val lastKnownLocation: Location?
        get() {
            val providers = locationManager!!.getProviders(true)
            var bestLocation: Location? = null
            for (provider in providers) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    requestLocationPermission()
                } else {
                    val l = locationManager!!.getLastKnownLocation(provider) ?: continue
                    if (bestLocation == null || l.accuracy < bestLocation.accuracy) {
                        bestLocation = l
                    }
                }
            }
            return bestLocation
        }

    fun addLocation() {
        if (isLocationPermissionGranted) {
            locationLayout!!.visibility = View.VISIBLE
            if (null != record!!.longitude && !record!!.longitude!!.isEmpty()) {
                startLocationIntentService()
            } else {
                addEditLocationName("")
            }
        } else {
            requestLocationPermission()
        }
    }

    enum class Editing {
        TIME_IN,
        TIME_OUT,
        BREAK
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_record_detail)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        Objects.requireNonNull<ActionBar>(supportActionBar).setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        MainActivity.isDataModified = false
        val fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setImageResource(R.drawable.ic_action_delete)
        fab.setOnClickListener { view ->
            mRecordViewModel.deleteRecord(record!!)
            //   deleteRecord(record.recordId);
            setResult(Activity.RESULT_OK, intent)
            finish()

        }
        val extras = intent.extras
        if (extras != null) {
            recordId = extras.getInt("RecordId")
        }
        displayedAddress = ""

        fullDate = findViewById(R.id.txtFullDate)
        fullDate!!.setOnClickListener { view -> adjustDateAndTime(record!!) }
        timeIn = findViewById(R.id.txtTimeIn)
        timeOut = findViewById(R.id.txtTimeOut)
        txtBreakTime = findViewById(R.id.txtBreakTime)
        note = findViewById(R.id.txtNote)
        note!!.setOnClickListener { addEditNote(note!!.text.toString()) }

        hoursWorked = findViewById(R.id.txtTotalHours)
        btnAddLocation = findViewById(R.id.btn_add_location)
        btnAddLocation!!.setOnClickListener {
            if (MyPreferences.getBool(MyPreferences.LOCATION_ON, false)) {
                addLocation()
            } else {
                showLocatioDialog()
            }
        }

        val recordObserver = Observer<RecordEntity> { recordEntity ->
            if (recordEntity != null) {
                record = recordEntity
                setUpDetailsLayout()
                setUpLocationView()
                if (record!!.latitude != null) {
                     if (MainActivity.isLocationRequested && !record!!.longitude!!.isEmpty() && record!!.longitude != "0.0" || !displayedAddress!!.isEmpty()) {
                        if (isLocationPermissionGranted) {
                            if (displayedAddress != record!!.address) {
                                startLocationIntentService()
                                locationLayout!!.visibility = View.VISIBLE
                            }
                        }
                    } else {
                        btnAddLocation!!.visibility = View.VISIBLE
                        hideLocationView()
                    }
                }
            }
        }
        mRecordViewModel.getRecordByIdLive(recordId).observe(this, recordObserver)
    }

    override fun onResume() {
        super.onResume()

    }
    fun showLocatioDialog() {
        val builder = AlertDialog.Builder(this)
        //builder.setTitle("Androidly Alert")
        builder.setMessage("You need to enable location services to use this functionality")
        //builder.setPositiveButton("OK", DialogInterface.OnClickListener(function = x))

        builder.setPositiveButton(R.string.enable) { dialog, which ->
            MyPreferences.setBool(MyPreferences.LOCATION_ON, true)
            addLocation()
        }

        builder.setNegativeButton(android.R.string.cancel) { dialog, which ->
            Toast.makeText(applicationContext,
                    android.R.string.no, Toast.LENGTH_SHORT).show()
        }

        builder.show()
    }

    private fun adjustDateAndTime(record: RecordEntity) {
        val mcurrentTime = Calendar.getInstance()
        val timeIn = record.timeIn
        mcurrentTime.timeInMillis = timeIn
        val year = mcurrentTime.get(Calendar.YEAR)
        val month = mcurrentTime.get(Calendar.MONTH)
        val day = mcurrentTime.get(Calendar.DAY_OF_MONTH)
        val hours = mcurrentTime.get(Calendar.HOUR_OF_DAY)
        val minutes = mcurrentTime.get(Calendar.MINUTE)
        val dlg = DatePickerDialog(this, { datePicker, i, i1, i2 ->
            Log.e("Day: ", "" + i + i2)
            mcurrentTime.set(i, i1, i2, hours, minutes)
            record.timeIn = mcurrentTime.timeInMillis
            this.mRecordViewModel.updateRecord(record)
        }, year, month, day)
        dlg.setTitle("Pick Day")
        dlg.show()

        dlg.setOnCancelListener { dialogInterface ->

        }

    }

    private fun hideLocationView() {
        locationLayout!!.visibility = View.GONE
    }

    private fun showLocationView() {
        locationLayout!!.visibility = View.VISIBLE
    }

    private fun setUpLocationView() {
        locationLayout = findViewById(R.id.layout_location)
        locationText = findViewById(R.id.txtLocation)
        locationText!!.isCursorVisible = false
        locationText!!.setOnClickListener { view -> addEditLocationName(locationText!!.text.toString()) }
        mapFragment = fragmentManager.findFragmentById(R.id.map) as MapFragment
        mapFragment.getMapAsync(this)
    }

    private fun setUpDetailsLayout() {
        fullDate!!.text = getDate(record!!.timeIn)
        timeIn!!.text = getFormatedTime(record!!.timeIn)
        timeOut!!.text = getFormatedTime(record!!.timeOut)
        val timeBreak = record!!.timeBreak
        val breakString = "$timeBreak minutes"
        txtBreakTime!!.text = breakString
        val breakInMillis = TimeUnit.MINUTES.toMillis(record!!.timeBreak.toLong())
        val durationInMillis = record!!.timeOut - record!!.timeIn
        getTimeDiff(durationInMillis)
        getTimeDiff(durationInMillis)
        val duration = MainActivity.round(getTimeDiff(durationInMillis - breakInMillis), 2).toString() + " hours"
        hoursWorked!!.text = duration
        note!!.text = record!!.notes
    }

    private fun startLocationIntentService() {
        if (!record!!.longitude!!.isEmpty() && !record!!.longitude!!.isEmpty()) {
            if (!Geocoder.isPresent()) {
                Toast.makeText(this,
                        R.string.no_geocoder_available,
                        Toast.LENGTH_LONG).show()
                return
            }
            locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            val location = lastKnownLocation
            if (location != null) {
                location.latitude = java.lang.Double.valueOf(record!!.latitude)!!
                location.longitude = java.lang.Double.valueOf(record!!.longitude)!!
                startIntentService(location, "")
            }
        } else {
            //Display set location button
        }
    }

    override fun onMapReady(gMap: GoogleMap) {
        googleMap = gMap
        googleMap.mapType = GoogleMap.MAP_TYPE_NORMAL
        try {
            googleMap.isMyLocationEnabled = true
        } catch (se: SecurityException) {

        }

        //Edit the following as per you needs
        googleMap.isTrafficEnabled = true
        googleMap.isIndoorEnabled = true
        googleMap.isBuildingsEnabled = true
        googleMap.uiSettings.isZoomControlsEnabled = true
        //
        if (record!!.latitude != null) {
            if (!record!!.latitude!!.isEmpty() && !record!!.longitude!!.isEmpty()) {
                val lat = java.lang.Double.parseDouble(record!!.latitude)
                val lon = java.lang.Double.parseDouble(record!!.longitude)
                val placeLocation = LatLng(lat, lon) //Make them global
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    requestLocationPermission()
                    return
                }
                googleMap.isMyLocationEnabled = true
                googleMap.mapType = GoogleMap.MAP_TYPE_NORMAL
                if (recordMapMarker != null)
                    recordMapMarker!!.remove()
                recordMapMarker = googleMap.addMarker(MarkerOptions()
                        .position(placeLocation)
                        .title("RecordEntity Location"))
                recordMapMarker!!.isDraggable = true
                googleMap.setOnMarkerDragListener(object : GoogleMap.OnMarkerDragListener {
                    override fun onMarkerDragStart(marker: Marker) {

                    }

                    override fun onMarkerDrag(marker: Marker) {

                    }

                    override fun onMarkerDragEnd(marker: Marker) {
                        record!!.latitude = marker.position.latitude.toString()
                        record!!.longitude = marker.position.longitude.toString()
                        mRecordViewModel.updateRecord(record!!)
                    }
                })
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(placeLocation))
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(10f), 1000, null)
            }
        }
    }

    override fun onPointerCaptureChanged(hasCapture: Boolean) {

    }

    fun editTimeIn(v: View) {
        editing = Editing.TIME_IN
        adjustTime(v)
    }

    fun editTimeOut(view: View) {
        editing = Editing.TIME_OUT
        adjustTime(view)
    }

    fun editBreak(view: View) {
        addBreakTime(record!!.timeBreak, record!!)
    }

    private fun adjustTime(v: View) {
        val mcurrentTime = Calendar.getInstance()
        val hour = mcurrentTime.get(Calendar.HOUR_OF_DAY)
        val minute = mcurrentTime.get(Calendar.MINUTE)
        val mTimePicker: TimePickerDialog
        when (editing) {
            RecordDetailActivity.Editing.TIME_IN -> timePickerTitle = getString(R.string.str_select_start_time)
            RecordDetailActivity.Editing.TIME_OUT -> timePickerTitle = getString(R.string.str_select_end_time)
        }

        mTimePicker = TimePickerDialog(this, { timePicker, selectedHour, selectedMinute ->
            var selectedMinuteString = selectedMinute.toString()
            if (selectedMinute < 10) {
                selectedMinuteString = "0$selectedMinute"
            }
            var adjustTimeString = "$selectedHour:$selectedMinuteString "
            val adjustTimeMillis: Long
            if (editing == Editing.TIME_IN) {
                adjustTimeString = getDate(record!!.timeIn) + " " + adjustTimeString
                adjustTimeMillis = MainActivity.getTimeInMillis(adjustTimeString)
                record!!.timeIn = adjustTimeMillis
                mRecordViewModel.updateRecord(record!!)
            } else {
                adjustTimeString = getDate(record!!.timeOut) + " " + adjustTimeString
                adjustTimeMillis = MainActivity.getTimeInMillis(adjustTimeString)
                record!!.timeOut = adjustTimeMillis
                mRecordViewModel.updateRecord(record!!)

            }
            timeIn!!.text = getFormatedTime(adjustTimeMillis)
        }, hour, minute, false)//Yes 24 hour time
        mTimePicker.setTitle("" + timePickerTitle!!)
        mTimePicker.show()

        mTimePicker.setOnCancelListener { dialogInterface ->

        }
    }

    override fun onFinishAddBreakDialog(breakTime: Int, recordId: Int) {
        record!!.timeBreak = breakTime
        mRecordViewModel.updateRecord(record!!)
        txtBreakTime!!.setText(String.format("%d minutes.", breakTime))
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED, intent)
        finish()
    }

    fun addEditNote(notes: String) {
        val builder = AlertDialog.Builder(this)
        builder.setCancelable(true)
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        //builder.setMessage(notes);
        val inflater = layoutInflater
        val editNoteLayout = inflater.inflate(R.layout.notes_editor_dialog, null)
        builder.setView(editNoteLayout)
        val editText = editNoteLayout.findViewById<View>(R.id.edit_note) as EditText
        var cBtnText: String? = null
        var pBtnText: String? = null

        builder.setTitle(getString(R.string.title_add_note))
        cBtnText = getString(R.string.btn_cancel)
        pBtnText = getString(R.string.btn_edit)
        //            editText.requestFocus();
        builder.setPositiveButton(pBtnText, null)
        builder.setNegativeButton(cBtnText) { dialogInterface, i ->
            imm?.hideSoftInputFromWindow(editText.windowToken, 0)
        }

        editText.setText(notes)

        editText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun afterTextChanged(editable: Editable) {
                if (editText.text.toString() != notes) {
                    alertDialog!!.getButton(AlertDialog.BUTTON_POSITIVE).visibility = View.VISIBLE
                    alertDialog!!.getButton(AlertDialog.BUTTON_POSITIVE).text = getString(R.string.btn_save)
                } else {
                    alertDialog!!.getButton(AlertDialog.BUTTON_POSITIVE).text = getString(R.string.btn_edit)
                }
            }
        })
        alertDialog = builder.create()
        //alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setText("");//setVisibility(View.INVISIBLE);
        alertDialog!!.setOnShowListener { dialog ->

            val button = (dialog as AlertDialog).getButton(AlertDialog.BUTTON_POSITIVE)
            button.setOnClickListener { view ->
                if (editText.text.toString() != notes) {
                    imm?.hideSoftInputFromWindow(editText.windowToken, 0)
                    record!!.notes = editText.text.toString()
                    mRecordViewModel.updateRecord(record!!)
                    alertDialog!!.dismiss()
                } else {
                    editText.isCursorVisible = true
                    imm?.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
                }
            }
        }

        editText.setOnFocusChangeListener { view, b ->

        }
        alertDialog!!.show()
        editText.clearFocus()
    }

    fun addEditLocationName(locationName: String) {
        val builder = AlertDialog.Builder(this)
        builder.setCancelable(true)
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        //builder.setMessage(notes);
        val inflater = layoutInflater
        val editNoteLayout = inflater.inflate(R.layout.notes_editor_dialog, null)
        builder.setView(editNoteLayout)
        val editText = editNoteLayout.findViewById<EditText>(R.id.edit_note)
        var cBtnText: String? = null
        var pBtnText: String? = null
        //editText.setCursorVisible(false);
        editText.clearFocus()
        builder.setTitle(getString(R.string.title_edit_location))
        cBtnText = getString(R.string.btn_cancel)
        pBtnText = getString(R.string.btn_edit)
        //            editText.requestFocus();
        builder.setPositiveButton(pBtnText, null)
        builder.setNegativeButton(cBtnText) { dialogInterface, i ->
            imm?.hideSoftInputFromWindow(editText.windowToken, 0)
        }

        editText.setText(locationName)

        editText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun afterTextChanged(editable: Editable) {
                if (editText.text.toString() != locationName) {
                    alertDialog!!.getButton(AlertDialog.BUTTON_POSITIVE).visibility = View.VISIBLE
                    alertDialog!!.getButton(AlertDialog.BUTTON_POSITIVE).text = getString(R.string.btn_save)
                } else {
                    alertDialog!!.getButton(AlertDialog.BUTTON_POSITIVE).text = getString(R.string.btn_edit)
                }
            }
        })
        alertDialog = builder.create()
        //alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setText("");//setVisibility(View.INVISIBLE);
        alertDialog!!.setOnShowListener { dialog ->

            val button = (dialog as AlertDialog).getButton(AlertDialog.BUTTON_POSITIVE)
            button.setOnClickListener { view ->
                if (editText.text.toString() != locationName) {
                    imm?.hideSoftInputFromWindow(editText.windowToken, 0)

                    if (editText.text.toString().isEmpty()) {
                        hideLocationView()
                        btnAddLocation!!.visibility = View.VISIBLE
                        record!!.latitude = ""
                        record!!.longitude = ""
                        mRecordViewModel.updateRecord(record!!)
                    } else {
                        locationText!!.text = editText.text.toString()
                        startIntentService(null, editText.text.toString())
                    }
                    alertDialog!!.dismiss()
                } else {
                    editText.isCursorVisible = true
                    imm?.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
                }
            }
        }

        editText.setOnFocusChangeListener { view, b ->

        }
        alertDialog!!.show()
        editText.clearFocus()
    }

    override fun displayAddressOutput(mAddressOutput: String) {
        btnAddLocation!!.visibility = View.GONE
        showLocationView()
        if (mAddressOutput.contains("Latitude")) {
            val coordinates = mAddressOutput.replace("Latitude: ", "").replace("Longitude: ", "")
            val parts = coordinates.split(",".toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray()
            val latitude = parts[0]
            val longitude = parts[1]
            record!!.latitude = latitude
            record!!.longitude = longitude
            record!!.address = address
            mRecordViewModel.updateRecord(record!!)
            val placeLocation = LatLng(java.lang.Double.valueOf(latitude)!!, java.lang.Double.valueOf(longitude)!!)
            //move marker
            if (recordMapMarker != null) {
                recordMapMarker!!.remove()
            }
            recordMapMarker = googleMap.addMarker(MarkerOptions()
                    .position(placeLocation)
                    .title("RecordEntity Location"))
            recordMapMarker!!.isDraggable = true
            googleMap.setOnMarkerDragListener(object : GoogleMap.OnMarkerDragListener {
                override fun onMarkerDragStart(marker: Marker) {

                }

                override fun onMarkerDrag(marker: Marker) {

                }

                override fun onMarkerDragEnd(marker: Marker) {
                    record!!.latitude = marker.position.latitude.toString()
                    record!!.longitude = marker.position.longitude.toString()
                    mRecordViewModel.updateRecord(record!!)
                    startLocationIntentService()
                }
            })

            googleMap.moveCamera(CameraUpdateFactory.newLatLng(placeLocation))
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(10f), 1000, null)
        } else {
            locationText!!.text = mAddressOutput
            record!!.address = mAddressOutput
            displayedAddress = mAddressOutput
            mRecordViewModel.updateRecord(record!!)
            initialLocationRequest = false
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main2, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId
        when (id) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }

    override fun requestLocationPermission() {
        // BEGIN_INCLUDE(camera_permission_request)
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)) {
                Snackbar.make(findViewById(R.id.main_content), R.string.permission_location_rationale,
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.ok) { view ->
                            ActivityCompat.requestPermissions(this,
                                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                                    REQUEST_LOCATION)
                        }
                        .show()

        } else {

            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    REQUEST_LOCATION)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {

        if (requestCode == REQUEST_LOCATION) {
            // Check if the only required permission has been granted
            if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // location has been granted
                MyPreferences.setBool(MyPreferences.LOCATION_ON, true)
                addLocation()
            } else {
                Snackbar.make(findViewById(R.id.main_content), resources.getString(R.string.permissions_not_granted),
                        Snackbar.LENGTH_SHORT).show()
                MyPreferences.setBool(MyPreferences.LOCATION_ON, false)
            }
        }
    }
}
