package com.apps.msaylica.worktimelog.data;

import android.app.Activity;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.apps.msaylica.worktimelog.R;
import com.apps.msaylica.worktimelog.ui.activities.MainActivity;
import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.util.Calendar;

/**
 * Created by msaylica on 2018-03-19.
 */

public class ExportData {

    private final MainActivity activity;
    private int recordCount;

    public ExportData(Activity activity) {
        this.activity = (MainActivity) activity;
    }
    // List<MyBean> beans comes from somewhere earlier in your code.

    public void exportTimeLogs() {
        activity.showHideProgress(MainActivity.PROGRESS_DETERMINATE, activity.getString(R.string.s_exporting));
        new ExportToCvsTask().execute();
    }

    private class ExportToCvsTask extends AsyncTask<Void, Integer, File> {

        @Override
        protected File doInBackground(Void... voids) {
            Cursor allRecords = activity.mRecordViewModel.getCursorAll();
            recordCount = allRecords.getCount();
            Calendar c = Calendar.getInstance();
            String timeInMillis = String.valueOf(c.getTimeInMillis());
            String FILENAME = "WorkTimeLog" + timeInMillis + ".csv";
            File directoryDownload = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            File logDir = new File(directoryDownload, FILENAME);
            try {
                if (logDir.createNewFile()) {
                    CSVWriter csvWriter = new CSVWriter(new FileWriter(logDir));
                    csvWriter.writeNext(allRecords.getColumnNames());
                    while (allRecords.moveToNext()) {
                        publishProgress((int) ((allRecords.getPosition() / (float) allRecords.getCount()) * 100));
                        String arrStr[] = {allRecords.getString(0), activity.getDateAndTime(allRecords.getString(1)),
                                activity.getDateAndTime(allRecords.getString(2)),
                                allRecords.getString(3), allRecords.getString(4), allRecords.getString(5),
                                allRecords.getString(6), allRecords.getString(7), allRecords.getString(8)
                                , allRecords.getString(9)};
                        csvWriter.writeNext(arrStr);
                    }
                    csvWriter.close();
                    allRecords.close();
                }
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }
            return logDir;
        }

        protected void onProgressUpdate(final Integer... progress) {
            activity.updateProgress(progress[0]);
        }

        protected void onPostExecute(final File result) {
            activity.showHideProgress(MainActivity.HIDE_PROGRESS, null);
            activity.exportCompleted(result, recordCount);
        }
    }

}
